<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerProducts extends Model
{
    protected $table = "partner_products";
	protected $primaryKey = "id";
}
