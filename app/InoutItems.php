<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InoutItems extends Model
{
    protected $table = "inout_items";
	protected $primaryKey = "id";
}
