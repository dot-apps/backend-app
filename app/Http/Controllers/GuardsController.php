<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use App\Guards;
use App\User;
use App\Role;

class GuardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guards = DB::table('guards')
                        ->orderBy('guards.created_at', 'ASC')
                        ->paginate(20);
        return view('guards.index', compact('guards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guards = Guards::all();
        return view('guards.create',compact('guards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'job' => 'required',
            'name' => 'required',
            'email' => 'required|unique:guards|unique:users',
            'phone' => 'required|unique:guards|unique:users',
            'dob' => 'required',
            'gender' => 'required',
            'rates' => 'required',
        ]);

        $guard = new guards;
        $guard->job = $request->job;
        $guard->name = $request->name;
        $guard->email = $request->email;
        $guard->phone = $request->phone;
        $guard->dob = $request->dob;
        $guard->gender = $request->gender;
        $guard->rates = $request->rates;

        if($request->hasFile('ktp')) {
            $guard->ktp = $request->ktp->store('guards', 'public');
        }
        if($request->hasFile('pic')) {
            $guard->pic = $request->pic->store('guards', 'public');
        }

        $guard->save();

        $pass = date_format(date_create($request->dob),'dmY');
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->birth_date = $request->dob;
        $user->gender = $request->gender;
        $user->country = 'GUARD';
        $user->password = Hash::make($pass);
        if($request->hasFile('ktp')) {
            $user->image = $request->ktp->store('profile_pics', 'public');
        }
        $user->guard_id = $guard->id;
        $user->save();

        $role = new Role;
        $role->user_id = $user->id;
        $role->role ='operator';
        $role->permission = 'Execute';
        $role->save();


        return redirect('guards')->with('msg_success', 'Guard Created Successfully');
        // dd($request);
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guard = Guards::find($id);
        return view('guards.view', compact('guard'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guards = Guards::all();
        $guard = Guards::findOrFail($id);
        return view('guards.edit', compact('guard','guards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'job' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'rates' => 'required',
        ]);

        $guard = Guards::find($id);
        $guard->job = $request->job;
        $guard->name = $request->name;
        $guard->email = $request->email;
        $guard->phone = $request->phone;
        $guard->dob = $request->dob;
        $guard->gender = $request->gender;
        $guard->rates = $request->rates;

        if($request->hasFile('ktp')) {
            $guard->ktp = $request->ktp->store('guards', 'public');
        }
        if($request->hasFile('pic')) {
            $guard->pic = $request->pic->store('guards', 'public');
        }

        $guard->save();

        $pass = date_format(date_create($request->dob),'dmY');
        $user = User::where('guard_id','=',$id)->firstOrFail();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->birth_date = $request->dob;
        $user->gender = $request->gender;
        $user->country = 'GUARD';
        $user->password = Hash::make($pass);
        if($request->hasFile('pic')) {
            $user->image = $request->pic->store('profile_pics', 'public');
        }
        $user->save();

        return redirect('guards')->with('msg_success', 'Guard Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $guard = Guards::find($id);
        $guard->delete();
        return redirect('guards')->with('msg_success', 'Guard Deleted Successfully');
    }
}
