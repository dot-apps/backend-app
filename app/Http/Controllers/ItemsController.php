<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Hashids\Hashids;
use DB;
use App\Products;
use App\Category;
use App\Items;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DB::table('items')
                        ->orderBy('items.created_at', 'DESC')
                        ->paginate(20);
        return view('items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = Products::find($product_id);
        return view('items.create', compact('categories','brands'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $x = 1;
        if($request->is_misc == 1 && $request->quantity) {
            $x = $request->quantity;
        }

        if(!$request->barcode) {
            $statement = DB::select("SHOW TABLE STATUS LIKE 'items'");
            $nextId = $statement[0]->Auto_increment;

            $hashids = new Hashids('', 6,'abcdefghijklmnopqrstuvwxyz123456789');
            echo $barcode = $hashids->encode($nextId);
        }
        else {
            $barcode = $request->barcode;
        }

        for ($i = 0; $i < $x; $i++) {

            $purchase_at = date_create($request->purchase_at);

            
            // $serial_number = \date_format($purchase_at,'ymd').'#'.str_pad($request->product_id,3,0,STR_PAD_LEFT).'#'.round(microtime(true) * 1000);


            $item = new Items;
            $item->product_id = $request->product_id;
            $item->barcode = strtoupper($barcode);
            $item->serial_number = strtoupper($request->serial_number);
            $item->desc = $request->desc;
            $item->purchase_at = $purchase_at;
            $item->save();
            
        }
        
        return redirect('inventories/'.$request->product_id)->with('msg_success', 'Item Created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $purchase_at = date_create($request->purchase_at);

        $item=Items::find($id);
        $item->product_id = $request->product_id;
        $item->serial_number = strtoupper($request->serial_number);
        $item->desc = $request->desc;
        $item->status = $request->status;
        $item->purchase_at = $purchase_at;
        $item->save();
        return redirect('inventories/'.$request->product_id)->with('msg_success', 'Item Created Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Items::find($id);
        $product_id = $item->product_id;
        $item->delete();
        return redirect('products/'.$product_id)->with('msg_success', 'Item Deleted Successfully');
        //
    }

    public function find(Request $request)
    {
        if($request->barcode) {
            $item = Items::select('items.*','products.name as product_name')->leftJoin('products','products.id','=','items.product_id')
            ->where('barcode','=',strtoupper($request->barcode))
            ->get();
        }
        if($request->res_type == 'json') {
            return \response()->json($item);
        }
        //
    }
}
