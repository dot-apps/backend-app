<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use DB;
use App\Operators;

class OperatorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operators = DB::table('operators')
                        ->orderBy('operators.created_at', 'DESC')
                        ->paginate(20);
        return view('operators.index', compact('operators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('operators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    // dd($request);
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:operators',
            'phone' => 'required|unique:operators',
            'fee' => 'required',
            'dob' => 'required',
        ]);

        $operator = new Operators;
        $operator->name = $request->name;
        $operator->email = $request->email;
        $operator->phone = $request->phone;
        $operator->dob = $request->dob;
        $operator->fee = $request->fee;
        if($request->hasFile('ktp')) {
            $operator->ktp = $request->ktp->store('operators', 'public');
        }
        if($request->hasFile('pic')) {
            $operator->pic = $request->pic->store('operators', 'public');
        }

        $operator->save();

        return redirect('operators')->with('msg_success', 'Operator Created Successfully');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $operator = Operators::find($id);
        return view('operators.view', compact('operator'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $operator = Operators::findOrFail($id);
        return view('operators.edit', compact('operator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'dob' => 'required',
        ]);

        $operator = Operators::find($id);
        $operator->name = $request->name;
        $operator->email = $request->email;
        $operator->phone = $request->phone;
        $operator->dob = $request->dob;
        $operator->fee = $request->fee;
        if($request->hasFile('ktp')) {
            $operator->ktp = $request->ktp->store('operators', 'public');
        }
        if($request->hasFile('pic')) {
            $operator->pic = $request->pic->store('operators', 'public');
        }

        $operator->save();

        return redirect('operators')->with('msg_success', 'Operator Edited Successfully');
        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $operator = Operators::find($id);
        $operator->delete();
        return redirect('operators')->with('msg_success', 'Operator Deleted Successfully');
    }
}
