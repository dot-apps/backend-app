<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionCarts extends Model
{
    protected $table = "transaction_carts";
	protected $primaryKey = "id";
}
