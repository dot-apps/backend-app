<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerItems extends Model
{
    protected $table = "partner_items";
	protected $primaryKey = "id";
}
