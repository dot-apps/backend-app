<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPics extends Model
{
    protected $table = "customer_pics";
	protected $primaryKey = "id";
}
