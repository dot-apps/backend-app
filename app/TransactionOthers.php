<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionOthers extends Model
{
    protected $table = "transaction_others";
	protected $primaryKey = "id";
}
