cd /var/www/admhcp.bungkusterus.com/backend-app
php artisan down
git pull
composer install --ignore-platform-reqs --optimize-autoloader
php artisan cache:clear
php artisan config:cache
php artisan route:cache
php artisan queue:restart
php artisan up
npm install
echo 'Deploy finished.'
exit