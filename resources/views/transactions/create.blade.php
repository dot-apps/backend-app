@extends('layouts.adminLayout')

@section('title')
New Product
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/transactions">Transactions</a></li>
    <li class="breadcrumb-item active">New Transaction</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-12">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add new transactions</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>

<form method="POST" action="/transactions" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div id="card-linear-color" class="card card-primary">
                <div class="card-header  ">
                    <div class="card-title font-weight-bold">
                        Customer Data
                    </div>
                    <div class="card-title pull-right font-weight-bold">
                        <a class="btn btn-info btn-sm text-light" id="btnCustFind" data-toggle="modal" data-target="#modalCust">FIND</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width:20%">Name</th>
                                <th>:</th>
                                <th style="width:80%">
                                    <input value="" class="form-control" name="customer_id" id="inputCustId" hidden/>
                                    <input value="" class="form-control" name="customer_type" id="inputCustType" hidden/>
                                    <input value="" class="form-control" name="customer_name" id="inputCustName" readonly/>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:10%">Phone</th>
                                <th>:</th>
                                <th style="width:90%">
                                    <input value="" class="form-control" name="customer_phone" id="inputCustPhone" readonly/>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:10%">Email</th>
                                <th>:</th>
                                <th style="width:90%">
                                    <input value="" class="form-control" name="customer_email" id="inputCustEmail" readonly/>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:10%">DOP</th>
                                <th>:</th>
                                <th style="width:90%">
                                    <input value="" class="form-control" name="customer_dop" />
                                </th>
                            </tr>
                            <tr>
                                <th style="width:10%">Gafferr</th>
                                <th>:</th>
                                <th style="width:90%">
                                    <input value="" class="form-control" name="customer_gaffer"/>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div id="card-linear-color" class="card card-primary">
                <div class="card-header  ">
                    {{-- <div class="card-title font-weight-bold">Dates</div> --}}
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width:25%">Production</th>
                                <th>:</th>
                                <th style="width:75%">
                                    <input type="text" value="" class="form-control" name="customer_production"/>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:25%">Location</th>
                                <th>:</th>
                                <th style="width:75%">
                                    <input type="text" value="" class="form-control" name="customer_location"/>
                                    <br>
                                    <span class="row justify-content-between">
                                        <input type="text" value="" class="form-control col-5" name="lat" placeholder="latitude"/>
                                        <input type="text" value="" class="form-control col-5" name="long" placeholder="longitude"/>
                                    </span>

                                </th>
                            </tr>
                            <tr>
                                <th style="width:25%">Equipment call</th>
                                <th>:</th>
                                <th style="width:75%">
                                    <input type="text" value="" class="form-control" name="customer_equipment_call"/>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:25%">Start to End Date</th>
                                <th>:</th>
                                <th style="width:75%">
                                    <div class="input-group row justify-content-between">
                                        <input type="date" class="input-sm form-control col-5" name="start_date"  />
                                        <div class="col-2 align-middle text-center">to</div>
                                        <input type="date" class="input-sm form-control col-5" name="end_date" />
                                    </div>
                                </th>
                            </tr>
                            <tr>
                                <th style="width:25%">Days</th>
                                <th>:</th>
                                <th style="width:75%">

                                        <input type="text" value="1" class="form-control-sm " name="days" readonly />
                                        Days

                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="card-linear-color" class="card card-primary">
                <div class="card-header  ">
                    <div class="card-title font-weight-bold">CART</div>
                    <div class="card-title pull-right font-weight-bold">
                        <a class="btn btn-info btn-sm text-light" id="btnCartModal" data-toggle="modal" data-target="#modalCart">+ADD</a>
                    </div>
                    <div class="card-title pull-right font-weight-bold mr-1">
                        <a class="btn btn-info btn-sm text-light" id="btnCartModal" data-toggle="modal" data-target="#modalCustomPackage">+CUSTOM PACKAGE</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="tblCart">
                        <thead>
                            <tr>
                                <th style="width:50%">Deksripsi</th>
                                <th style="width:8%" class="text-center">Unit</th>
                                <th style="width:8%" class="text-center">Hari</th>
                                <th style="width:17%" class="text-center">Harga Satuan</th>
                                <th style="width:17%" class="text-center">Harga perhari</th>
                                <th style="width:17%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <table class="table table-bordered" id="tblCartSum">
                        <tbody>
                            <tr>
                                <td class="text-right " style="width:76%">
                                    SUBTOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center cartSubtotal" name="cart_subtotal" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    Discount (per 7 DAYS)
                                </td>
                                <td  style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center cartTotalDiscount" name="cart_discount" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    Add. Discount
                                    <br>
                                    <input type="number" value="0" min="0" class="text-center form-control col-sm-1 pull-right cartAddDiscount" name="cart_add_discount"/>
                                    <br>
                                    (%)
                                </td>
                                <td  style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center cartAddDiscountAmount" name="cart_add_discount_amount" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    TOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center cartTotalAmount" name="cart_total" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="card-linear-color" class="card card-primary">
                <div class="card-header  ">
                    <div class="card-title font-weight-bold">GUARD(S)</div>
                    <div class="card-title pull-right font-weight-bold">
                        <a class="btn btn-info btn-sm text-light" id="btnGuardsModal" data-toggle="modal" data-target="#modalGuards">+ADD</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="tblGuards">
                        <thead>
                            <tr>
                                <th style="width:50%">Deksripsi</th>
                                <th style="width:8%" class="text-center">Unit</th>
                                <th style="width:8%" class="text-center">Hari</th>
                                <th style="width:17%" class="text-center">Harga Satuan</th>
                                <th style="width:17%" class="text-center">Harga perhari</th>
                                <th style="width:17%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <table class="table table-bordered" id="tblGuardsSum">
                        <tbody>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    TOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center guardsTotalAmount" name="guards_total" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="card-linear-color" class="card card-primary">
                <div class="card-header  ">
                    <div class="card-title font-weight-bold">OTHERS</div>
                    <div class="card-title pull-right font-weight-bold">
                        <a class="btn btn-info btn-sm text-light" id="btnOthersModal" data-toggle="modal" data-target="#modalOthers">+ADD</a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="tblOthers">
                        <thead>
                            <tr>
                                <th style="width:50%">Deksripsi</th>
                                <th style="width:8%" class="text-center">Unit</th>
                                <th style="width:17%" class="text-center">Harga Satuan</th>
                                <th style="width:17%" class="text-center">Total</th>
                                <th style="width:17%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <table class="table table-bordered" id="tblGuardsSum">
                        <tbody>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    TOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center othersTotalAmount" name="others_total" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div id="card-linear-color" class="card card-primary">
                <div class="card-header  ">
                    <div class="card-title font-weight-bold">SUMMARY</div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="tblGuardsSum">
                        <tbody>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    CART TOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center cartTotalAmount" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    GUARDS TOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center guardsTotalAmount" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    OTHERS TOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center othersTotalAmount" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    TAX PPN (10%)
                                </td>
                                <td class="" style="width: 16%">
                                    <input name="tax_total" type="number" value="0" min="1" class="form-control text-center taxTotalAmount" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="width:76%">
                                    GRAND TOTAL
                                </td>
                                <td class="" style="width: 16%">
                                    <input type="number" value="0" min="1" class="form-control text-center grandTotal" name="total" readonly/>
                                </td>
                                <td style="width: 17%"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block">Create</button>
</form>

<!-- CUSTOMER MODAL -->
<div class="modal left fade" id="modalCust" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Customer Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for=""></label>
                    <input type="text" placeholder="please input customer phone" class="form-control" id="mdlCustInputFind"/>
                </div>
                <div class="col-md-12 text-center">
                    <a class="btn btn-primary " id="btnCustFind">FIND</a>
                </div>
                <br>
                <div class="col-md-12 text-center">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="tblCustFindResult">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CUSTOMER -->

<div class="modal right fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Price List</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="searchTablePriceList" class="form-control pull-right" placeholder="Search">
                <br>
                <br>
                <table class="table table-hover" id="tablePriceList">
                    <thead>
                        <tr>
                            <th class="col-md-12 font-weight-bold">Product</th>
                            <th class="col-md-3 text-right font-weight-bold">1 Day</th>
                            <th class="col-md-3 text-right font-weight-bold">7 Days</th>
                            <th class="text-center font-weight-bold">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $id=0 ?>
                        @foreach($list as $_list)

                        <tr>
                            <td id="plDesc{{$id}}">
                                {{$_list->name}}<br>
                                @if($_list->type == 2)
                                    <ul>
                                    <?php $i = json_decode($_list->package_product,TRUE) ?>
                                    @foreach($i as $_i)
                                        <li>{{$_i['category']}} {{$_i['brand']}} {{$_i['name']}} x {{$_i['qty']}}</li>
                                    @endforeach
                                    </ul>
                                @endif
                            </td>
                            <td id="plPriceDay{{$id}}" class="text-right" data-int="{{$_list->price_day}}">
                                Rp.{{number_format($_list->price_day)}}
                            </td>
                            <td id="plPrice7Days{{$id}}" class="text-right" data-int="{{$_list->price_7days}}">
                                Rp.{{number_format($_list->price_7days)}}
                            </td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-pick-pl" data-id="{{$id}}" data-prod-uid="{{$_list->uid}}" data-prod-type="{{$_list->type}}">
                                    Pick
                                </a>
                            </td>
                        </tr>
                        <?php $id++ ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>
<div class="modal left fade" id="modalCustomPackage" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-xl" style=" max-width:85% !important; width: 85% !important">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Custom Package</h4>
            </div>
            <div class="modal-body" style="padding: 25px !important">
                <div class="row">
                    <div class="col-sm-5 mt-1">
                        <div class="card-title">Select Product</div>
                        <input type="text" id="searchTableProductList" class="form-control pull-right mb-2" placeholder="Search">
                        <table class="table table-hover" id="tableProductList">
                            <thead>
                                <tr>
                                    <th class="col-md-12 font-weight-bold">Product</th>
                                    <th class="col-md-3 text-right font-weight-bold">1 Day</th>
                                    <th class="col-md-3 text-right font-weight-bold">7 Days</th>
                                    <th class="text-center font-weight-bold">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $id=0 ?>
                                @foreach($list as $_list)
                                @if($_list->type == 1)
                                <tr>
                                    <td id="plcpDesc{{$_list->uid}}">
                                        {{$_list->name}}<br>
                                    </td>
                                    <td id="plcpPriceDay{{$_list->uid}}" class="text-right" data-int="{{$_list->price_day}}">
                                        Rp.{{number_format($_list->price_day)}}
                                    </td>
                                    <td id="plcpPrice7Days{{$_list->uid}}" class="text-right" data-int="{{$_list->price_7days}}">
                                        Rp.{{number_format($_list->price_7days)}}
                                    </td>
                                    <td class="text-center">
                                        {{-- <input type="checkbox" class="form-check-input checkSelectProduct" name="check_custom_package[]"  value="{{ $_list->uid }}"> --}}
                                        <a class="btn btn-primary btn-sm checkSelectProduct" data-id="{{$_list->uid}}" data-prod-uid="{{$_list->uid}}" data-prod-type="{{$_list->type}}">
                                            Pick
                                        </a>
                                    </td>
                                </tr>
                                <?php $id++ ?>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-7" id="pdlc">
                        <div class="card-title">Detail Pacakge</div>
                        <div class="form-group form-group-default required">
                            <label for="full_name">Price per Day</label>
                            <input type="number" name="price_day" min=1000 class="form-control @error('price_day') is-invalid @enderror" id="price_day" value="{{ old('price_day') }}" required>
                        </div>
                        <div class="form-group form-group-default required">
                            <label for="full_name">Price 7 Days</label>
                            <input type="number" name="price_7days" min=1000 class="form-control @error('price_7days') is-invalid @enderror" id="price_7day" value="{{ old('price_7day') }}" required>
                        </div>
                        <table class="table" id="tblProductCustomPackagePicked">
                            <thead>
                                <tr>
                                    <th style="width:80%">Product</th>
                                    <th style="width:10%">Quantity</th>
                                    <th style="width:10%">Action</th>
                                </tr>
                            </thead>
                            <tbody id="tbodyProductCustomPackagePicked"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submitCheckPackage">Submit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal right fade" id="modalGuards" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Guards List</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="searchTableGuardsList" class="form-control pull-right" placeholder="Search">
                <br>
                <br>
                <table class="table table-hover" id="tableGuardsList">
                    <thead>
                        <tr>
                            <th class=" font-weight-bold">Job</th>
                            <th class=" font-weight-bold">Name</th>
                            <th class="text-right font-weight-bold">1 Day</th>
                            <th class="text-center font-weight-bold">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $id=0 ?>
                        @foreach($guards as $_guard)

                        <tr>
                            <td id="glDesc{{$_guard->id}}">
                                {{$_guard->job}}
                            </td>
                            <td id="glName{{$_guard->id}}">
                                {{$_guard->name}}
                            </td>
                            <td id="glPriceDay{{$_guard->id}}" class="text-right" data-int="{{$_guard->rates}}">
                                Rp.{{number_format($_guard->rates)}}
                            </td>
                            <td class="text-center">
                                <a class="btn btn-primary btn-pick-gl" data-id="{{$_guard->id}}">Pick</a>
                            </td>
                        </tr>
                        <?php $id++ ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div> -->
        </div>
    </div>
</div>

<!-- OTHERS MODAL -->
<div class="modal fade" id="modalOthers" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Others</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group form-group-default required">
                    <label for="full_name">Desc</label>
                    <input style="" type="text" id="otherNameInput" class="form-control" >
                </div>
                <div class="form-group form-group-default required">
                    <label for="full_name">Price</label>
                    <input style="" type="number" id="otherPriceInput" class="form-control"  min=0>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-primary" id="btnOtherSubmit">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- OTHERS -->


<style>
input[readonly] {
    background-color: #e5ebf9 !important;
    color:black !important;
}
</style>
<script>
    var customers = <?=json_encode($customers)?>;
    $('#modalCust').on('click','#btnCustFind',function(){
        var custToFind = $('#mdlCustInputFind').val();
        var custResults = []
        var custFound=false
        var html = ''
        for(var key in customers) {
            var custLoop = customers[key]

            if(isNaN(custToFind)) {
                var ctf = new RegExp(custToFind,'gi')
                if(String(custLoop.name).match(ctf)) {
                    html += '<tr>'+
                    '   <td class="text-left">'+((custLoop.type == 1) ? 'Personal' : 'Corporate')+'</td>'+
                    '   <td class="text-left">'+custLoop.name+'</td>'+
                    '   <td class="text-left">'+custLoop.phone+'</td>'+
                    '   <td class="text-left">'+custLoop.email+'</td>'+
                    '   <td class="text-left">'+custLoop.address+'</td>'+
                    '   <td class="text-left">'+
                    '       <button type="button" class="btn btn-primary btnCustChoose" data-custkey="'+key+'">Choose</button>'+
                    '   </td>'+
                    '</tr>';
                    // $('#btnCustChoose').data('custData', custLoop)
                    custFound = true
                }

            }
            else {
                if(custLoop.phone == custToFind) {
                    html += '<tr>'+
                    '   <td class="text-left">'+((custLoop.type == 1) ? 'Personal' : 'Corporate')+'</td>'+
                    '   <td class="text-left">'+custLoop.name+'</td>'+
                    '   <td class="text-left">'+custLoop.phone+'</td>'+
                    '   <td class="text-left">'+custLoop.email+'</td>'+
                    '   <td class="text-left">'+custLoop.address+'</td>'+
                    '   <td class="text-left">'+
                    '       <button type="button" class="btn btn-primary btnCustChoose" data-custkey="'+key+'">Choose</button>'+
                    '   </td>'+
                    '</tr>';
                    custFound = true
                }
            }
        }

        if(!custFound) {
            html += '<tr>'+
            '   <td colspan=6 class="text-danger">Customer data not found</td>'+
            '</tr>';
        }

        console.log(custFound)
        console.log(html)

        $('#tblCustFindResult').html(html)
    })
    $('#modalCust').on('click','.btnCustChoose',function(){
        var custKey = $(this).data('custkey')
        var custData = customers[custKey]
        $('#inputCustId').val(custData.id)
        $('#inputCustType').val(custData.type)
        $('#inputCustName').val(custData.name)
        $('#inputCustPhone').val(custData.phone)
        $('#inputCustEmail').val(custData.email)
        $('#inputCustAddress').val(custData.address)
        $('#modalCust').modal('hide')
    })
</script>
<script>
    var qtyDay = 1
    function calcDays() {
        var startDate = moment($('input[name=start_date]').val(),'YYYY-MM-DD')
        var endDate = moment($('input[name=end_date]').val(),'YYYY-MM-DD')
        qtyDay = endDate.diff(startDate,'days')+1;
        $('input[name=days]').val(qtyDay)
        $('input[name="product_day[]"').val(qtyDay)
        $('input[name="guard_day[]"').val(qtyDay)
        calcCartCost()
        calcGuardsCost();
        calcGrandTotal()
    }
    $('input[name=start_date]').on('change',function () {
        $('input[name=end_date').attr('min', $(this).val())
    })
    $('input[name=end_date').on('change', function() {
        calcDays()
        /* Act on the event */
    });

</script>
<script>
    function calcGrandTotal() {
        var totalCart = $('.cartTotalAmount').val();
        var totalGuards = $('.guardsTotalAmount').val();
        var totalOthers = $('.othersTotalAmount').val();
        var grandTotal = parseInt(totalCart)+parseInt(totalGuards)+parseInt(totalOthers)
        var tax = grandTotal/100*10
        grandTotal = grandTotal+tax
        $('.taxTotalAmount').val(tax)
        $('.grandTotal').val(grandTotal)
    }
    calcGrandTotal()
</script>
<script>
    function calcCartCost() {
        var subtotal = 0;
        var discounts = 0;
        var addDiscounts = 0;
        var total = 0;
        $('#tblCart tr').each( function() {
            if ( $(this).find('td').length && $(this).find('td input').length ) {
                var quant = parseInt($(this).find('td input').eq(2).val())
                var days = parseInt($(this).find('td input').eq(3).val())
                var price = parseInt($(this).find('td input').eq(4).val());
                var amount = quant * days * price
                $(this).find('.amount').val(amount);
                subtotal = subtotal+amount

                var discount = 0
                if(days >= 7) {
                    var price7Days =  parseInt($(this).find('td input').eq(4).data('7days'))
                    var division = parseInt(days/7)
                    var modulus = days%7

                    var totPrice7Days = division*price7Days
                    var totModulus = modulus*price
                    var tot = totPrice7Days+totModulus
                    var discount = tot-amount

                    discounts=discounts+discount
                }

                amount = amount+discount

                if($('.cartAddDiscount').val() != 0) {
                    var cartAddDiscount = $('.cartAddDiscount').val()
                    cartAddDiscount = parseInt((cartAddDiscount/100) * amount)
                    addDiscounts = -(addDiscounts+cartAddDiscount)
                    amount = amount - cartAddDiscount
                }

                total = total+amount
            }
        });
        // console.log(addDiscounts);
        $('.cartSubtotal').val(subtotal);
        $('.cartTotalDiscount').val(discounts);
        $('.cartAddDiscountAmount').val(addDiscounts);
        $('.cartTotalAmount').val(total);
    }

    calcCartCost();

    $('#tblCart').on('change','input', function() {
        calcCartCost();
        calcGrandTotal()
    });

    $('.cartAddDiscount').on('change', function(event) {
        calcCartCost();
        calcGrandTotal()
    });

    var initTablePriceList = function() {
        var tablePriceList = $('#tablePriceList');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            // "oLanguage": {
            //     "sLengthMenu": "_MENU_ ",
            //     "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            // },
            "iDisplayLength": 10
        };

        tablePriceList.dataTable(settings);

        // search box for table
        $('#searchTablePriceList').keyup(function() {
            tablePriceList.fnFilter($(this).val());
        });

        $('.btn-pick-pl').click(function(event) {
            var rowId = $(this).data('id')
            var html = '<tr>'+
            '<td>'+$('#plDesc'+rowId).html()+'</td>'+
            '<td>'+
            '<textarea name="product_name[]" class="hidden" readonly>'+$('#plDesc'+rowId).html()+'</textarea>'+
            '<input type="text" value="'+$(this).data('prod-uid')+'"  class="form-control text-center hidden" name="product_uid[]" readonly/>'+
            '<input type="text" value="'+$(this).data('prod-type')+'"  class="form-control text-center hidden" name="product_type[]" readonly/>'+
            '<input type="number" value="1" min="1" class="form-control text-center" name="product_qty[]"/>'+
            '</td>'+
            '<td><input type="number"  min="1" class="form-control text-center" name="product_day[]" value="'+qtyDay+'" readonly/></td>'+
            '<td><input type="number" data-day="'+$('#plPriceDay'+rowId).data('int')+'" data-7days="'+$('#plPrice7Days'+rowId).data('int')+'" value="'+$('#plPriceDay'+rowId).data('int')+'" min="1" class="form-control text-center" name="product_price[]" readonly/></td>'+
            '<td><input type="number" value="'+$('#plPriceDay'+rowId).data('int')+'" min="1" class="form-control text-center amount" name="product_total[]" readonly/></td>'+
            '<td><a href="#" class="btn btn-danger btn-remove-row">REMOVE</a></td>'+
            '</tr>'

            $('#tblCart').find('tr:last').after(html)
            calcCartCost();
            calcGrandTotal()
        });

        $('#tblCart').on('click','td .btn-remove-row',function(e){
            e.preventDefault();
            $(this).parents('tr').remove();
            calcCartCost();
            calcGrandTotal()
        });
    }
    initTablePriceList()
</script>
<script>
    function calcGuardsCost() {
        var total = 0;
        $('#tblGuards tr').each( function() {
            if ( $(this).find('td').length && $(this).find('td input').length ) {
                var quant = parseInt($(this).find('td input').eq(1).val())
                var days = parseInt($(this).find('td input').eq(2).val())
                var price = parseInt($(this).find('td input').eq(3).val());
                var amount = quant * days * price
                $(this).find('.amount').val(amount);
                total = total+amount
            }
        });
        $('.guardsTotalAmount').val(total);
    }

    calcGuardsCost();
    calcGrandTotal()

    $('#tblGuards').on('change','input', function() {
        calcGuardsCost();
        calcGrandTotal()
    });

    var initTableGuardsList = function() {
        var tableGuardsList = $('#tableGuardsList');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            // "oLanguage": {
            //     "sLengthMenu": "_MENU_ ",
            //     "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            // },
            "iDisplayLength": 10
        };

        tableGuardsList.dataTable(settings);

        // search box for table
        $('#searchTableGuardsList').keyup(function() {
            tableGuardsList.fnFilter($(this).val());
        });

        $('.btn-pick-gl').click(function(event) {
            var rowId = $(this).data('id')
            var html = '<tr>'+
            '<td>'+$('#glDesc'+rowId).html()+
            '<textarea name="guard_name[]" class="hidden" readonly>'+$('#glName'+rowId).html()+'</textarea>'+
            '<textarea name="guard_job[]" class="hidden" readonly>'+$('#glDesc'+rowId).html()+'</textarea>'+
            '<input type="text" value="'+rowId+'"  class="form-control text-center hidden" name="guard_id[]"/>'+
            '</td>'+
            '<td><input type="number" value="1" min="1" class="form-control text-center" name="guard_qty[]"/></td>'+
            '<td><input type="number" min="1" class="form-control text-center" name="guard_day[]"  value="'+qtyDay+'" readonly/></td>'+
            '<td><input type="number" data-day="'+$('#glPriceDay'+rowId).data('int')+'" value="'+$('#glPriceDay'+rowId).data('int')+'" min="1" class="form-control text-center" name="guard_price[]" readonly/></td>'+
            '<td><input type="number" value="'+$('#glPriceDay'+rowId).data('int')+'" min="1" class="form-control text-center amount" name="guard_total[]" readonly/></td>'+
            '<td><a href="#" class="btn btn-danger btn-remove-row">REMOVE</a></td>'+
            '</tr>'

            $('#tblGuards').find('tr:last').after(html)
            calcGuardsCost();
            calcGrandTotal()
        });

        $('#tblGuards').on('click','td .btn-remove-row',function(e){
            e.preventDefault();
            $(this).parents('tr').remove();
            calcGuardsCost();
            calcGrandTotal()
        });
    }
    initTableGuardsList()
</script>
<script>
    function calcOthersCost() {
        var total = 0;
        $('#tblOthers tr').each( function() {
            if ( $(this).find('td').length && $(this).find('td input').length ) {
                var quant = parseInt($(this).find('td input').eq(0).val())
                var price = parseInt($(this).find('td input').eq(1).val());
                var amount = quant * price
                $(this).find('.amount').val(amount);
                total = total+amount
            }
        });
        $('.othersTotalAmount').val(total);
    }

    calcOthersCost();
    calcGrandTotal()

    $('#tblOthers').on('change','input', function() {
        calcOthersCost();
        calcGrandTotal()
    });

    $('#btnOtherSubmit').on('click', function(event) {

        var otherName = $('#otherNameInput').val()
        var otherPrice = $('#otherPriceInput').val()

        var rowId = $(this).data('id')
        var html = '<tr>'+
        '<td>'+otherName+
        '   <textarea name="other_name[]" class="hidden" readonly>'+otherName+'</textarea>'+
        '</td>'+
        '<td><input type="number" value="1" min="1" class="form-control text-center" name="other_qty[]"/></td>'+
        '<td><input type="number" min="1" class="form-control text-center" name="other_price[]" value="'+otherPrice+'" readonly/></td>'+
        '<td><input type="number" min="1" class="form-control text-center amount" name="other_total[]" value="'+otherPrice+'" readonly/></td>'+
        '<td><a href="#" class="btn btn-danger btn-remove-row">REMOVE</a></td>'+
        '</tr>'

        $('#tblOthers').find('tr:last').after(html)
        calcOthersCost();
        calcGrandTotal();

        $('#tblOthers').on('click','td .btn-remove-row',function(e){
            e.preventDefault();
            $(this).parents('tr').remove();
            calcOthersCost();
            calcGrandTotal()
        });
    });
</script>
<script>
    var initTableProductList = function() {
        var tableProductList = $('#tableProductList');

        var settings = {
            "sDom": "<t><'row'<p i>>",
            "destroy": true,
            "scrollCollapse": true,
            // "oLanguage": {
            //     "sLengthMenu": "_MENU_ ",
            //     "sInfo": "Showing <b>_START_ to _END_</b> of _TOTAL_ entries"
            // },
            "iDisplayLength": 10
        };

        tableProductList.dataTable(settings);

        // search box for table
        $('#searchTableProductList').keyup(function() {
            tableProductList.fnFilter($(this).val());
        });

        $('.checkSelectProduct').click(function(event) {
            var rowId = $(this).data('id');
            var html = '<tr id="rowProd'+rowId+'">'
            html +='<td style="width:80%"><input name="products[]" type="text" value="'+rowId+'" class="hidden">'+$('#plcpDesc'+rowId).html()+'</td>'
            html +='<td style="width:10%"><input name="qty[]" style="width:70%" type="number" min="1" value="1"></td>'
            html +='<td style="width:10%"><a class="btn btn-danger btn-sm text-light btnRemoveProduct" data-id="'+rowId+'">Remove</a><td>'
            html +='</tr>'

            $('#tbodyProductCustomPackagePicked').append(html)
            // calcCartCost();
            // calcGrandTotal()
        });

        $('#tblProductCustomPackagePicked').on('click','td .btnRemoveProduct',function(e){
            e.preventDefault();
            $(this).parents('tr').remove();
            calcCartCost();
            calcGrandTotal()
        });

        $('#submitCheckPackage').on('click',function() {
            var modal = $('#modalCustomPackage')
            var priceDay = modal.find('input[name=price_day]').val()
            var price7Days = modal.find('input[name=price_7days]').val()

            var products =  []
            modal.find('input[name="products[]"]').each(function(key,val) {
                products.push($(this).val())
            })

            var qtys =  []
            modal.find('input[name="qty[]"]').each(function(key,val) {
                qtys.push($(this).val())
            })


            let dataPost = {
                _token: "{{ csrf_token() }}",
                name: 'CUSTOM-'+Date.now(),
                price_day: priceDay,
                price_7days: price7Days,
                products: products,
                is_custom: 1,
                qty:qtys
            }
            console.log(dataPost)

            $.post("{{ url('packages') }}", dataPost ,function(res){
                if(res.code == 200) {
                    let data = res.data
                    let packProd = JSON.parse(data.package_product)

                    let detail = '<ul>'
                    for(let pp of packProd) {
                        console.log(pp)
                        detail+="<li> "
                        detail+=" "+pp.category
                        detail+=" "+pp.brand
                        detail+=" "+pp.name
                        detail+=" x "+pp.qty
                    }
                    detail +='</ul>'
                    var html = '<tr>'+
                    '<td>'+
                        data.name+
                        detail+
                    '</td>'+
                    '<td>'+
                    '   <textarea name="product_name[]" class="hidden" readonly>'+data.name+'</textarea>'+
                    '   <input type="text" value="'+data.uid+'"  class="form-control text-center hidden" name="product_uid[]" readonly/>'+
                    '   <input type="text" value="'+data.type+'"  class="form-control text-center hidden" name="product_type[]" readonly/>'+
                    '   <input type="number" value="1" min="1" class="form-control text-center" name="product_qty[]"/>'+
                    '</td>'+
                    '<td>'+
                    '   <input type="number"  min="1" class="form-control text-center" name="product_day[]" value="'+qtyDay+'" readonly/>'+
                    '</td>'+
                    '<td>'+
                    '   <input type="number" data-day="'+data.price_day+'" data-7days="'+data.price_7days+'" value="'+data.price_day+'" min="1" class="form-control text-center" name="product_price[]" readonly/>'+
                    '</td>'+
                    '<td>'+
                    '   <input type="number" value="'+data.price_day+'" min="1" class="form-control text-center amount" name="product_total[]" readonly/>'+
                    '</td>'+
                    '<td>'+
                    '   <a href="#" class="btn btn-danger btn-remove-row">REMOVE</a>'+
                    '</td>'+
                    '</tr>'

                    $('#tblCart').find('tr:last').after(html)
                    $('#tblProductCustomPackagePicked tbody').empty();
                    modal.find('input[name=price_day]').val('');
                    modal.find('input[name=price_7days]').val('');
                    calcCartCost();
                    calcGrandTotal()
                    modal.modal('hide')
                }
            });
        })
    }
    initTableProductList()
</script>
@endsection
