@extends('layouts.adminLayout')

@section('title')
Transaction Detail
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/transactions">transactions</a></li>
    <li class="breadcrumb-item active">transaction Detail</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <span class="h6">
            <b>#{{ $transaction->number }}</b>
            <br>
            <br>
            <button type="button" class="btn btn-default btn-cons" data-toggle="modal" data-target="#modalUpdateStatus">
                Status: {{ $transaction->status_name }}
            </button>
            <button type="button" class="btn btn-default btn-cons" data-toggle="modal" data-target="#modalUpdatePaymentStatus">
                Payment: {{ $transaction->payment_status_name }}
            </button>
        </span>
        <br>
    </div>
    <div class="col-md-6 text-right">
        <a href="{{ '/transactions/dokumen/'.$transaction->id.'/unduh' }}" class="btn btn-outline-primary">Download PDF</a>
        <a href="{{ '/transactions/dokumen/'.$transaction->id.'/email' }}" class="btn btn-outline-primary">Send Email</a>
        <a href="{{ '/transactions/'.$transaction->id.'/edit' }}" class="btn btn-outline-primary">Edit Data</a>
    </div>

    <hr class="col-md-12" style="margin-top: 0.5rem !important; margin-bottom: 0.7rem">
</div>
<div class="row">
    <div class="col-xl-12">
        <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left " id="tab-3">
            <li class="nav-item">
                <a href="#" class="active" data-toggle="tab" data-target="#home">
                    Cart
                </a>
            </li>
            <li class="nav-item">
                <a href="#" data-toggle="tab" data-target="#payment" >
                    Payment
                </a>
            </li>
            <li class="nav-item">
                <a href="#" data-toggle="tab" data-target="#inventory" class="">
                    Inventory
                </a>
            </li>
            <li class="nav-item">
                <a href="#" data-toggle="tab" data-target="#guard" class="">
                    Guard
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="home">
                <div class="row">
                    <div class="col-md-6">
                        <div id="card-linear-color" class="card card-default">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">
                                    Customer Data
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    @php
                                        $customer = json_decode($transaction->customers,TRUE);
                                    @endphp
                                    <thead>
                                        <tr>
                                            <th style="width:20%">Name</th>
                                            <th>:</th>
                                            <th style="width:80%">
                                                {{$customer['customer_name']}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">Phone</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                {{$customer['customer_phone']}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">Email</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                {{$customer['customer_email']}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">DOP</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                {{$customer['customer_dop']}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:10%">Gaffer</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                {{$customer['customer_gaffer']}}
                                            </th>
                                        </tr>
                                        {{-- <tr>
                                            <th style="width:10%">Address</th>
                                            <th>:</th>
                                            <th style="width:90%">
                                                {{$customer['customer_address']}}
                                            </th>
                                        </tr> --}}
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="card-linear-color" class="card card-default">
                            <div class="card-header  ">
                                {{-- <div class="card-title font-weight-bold">Dates</div> --}}
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width:25%">Production</th>
                                            <th>:</th>
                                            <th style="width:75%">
                                                {{$customer['customer_production']}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:25%">Location</th>
                                            <th>:</th>
                                            <th style="width:75%">
                                                {{$customer['customer_location']}}
                                                <br>
                                                ({{$customer['customer_location_lat']}},{{$customer['customer_location_long']}})
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:25%">Equipment Call</th>
                                            <th>:</th>
                                            <th style="width:75%">
                                                {{$customer['customer_equipment_call']}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:25%">Start End Date</th>
                                            <th>:</th>
                                            <th style="width:75%">
                                                {{ $transaction->start_date }} - {{ $transaction->end_date }}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="width:25%">Total Days</th>
                                            <th>:</th>
                                            <th style="width:75%">
                                                {{ $transaction->days }}
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-default col-md-12">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">Items</div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tblCart">
                                    <thead>
                                        <tr>
                                            <th style="width:50%">Deksripsi</th>
                                            <th style="width:8%" class="text-center">Unit</th>
                                            <th style="width:8%" class="text-center">Hari</th>
                                            <th style="width:17%" class="text-center">Harga Satuan</th>
                                            <th style="width:17%" class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($carts as $_cart)
                                        <tr>
                                            <td>{!! $_cart->product_name !!}</td>
                                            <td class="text-center">{!!$_cart->qty !!}</td>
                                            <td class="text-center">{!!$_cart->day !!}</td>
                                            <td class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($_cart->price)}}</td>
                                            <td class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($_cart->total)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                <table class="table table-bordered" id="tblCartSum">
                                    <tbody>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                SUBTOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                Rp.{{number_format($transaction->cart_subtotal)}}
                                            </td>
                                        </tr>
                                        @php
                                            $cart_discounts = json_decode($transaction->cart_discounts,TRUE);
                                            $disc7D = $cart_discounts[0][0];
                                            $discAdd = $cart_discounts[0][1];
                                        @endphp
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                DISCOUNT (> 7 Days)
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format(str_replace('-','',$disc7D['amount']))}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                DISCOUNT ({{$discAdd['percentage']}}%)
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                ({{number_format(str_replace('-','',$discAdd['amount']))}})
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                TOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->cart_total)}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-default col-md-12">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">Guards</div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tblCart">
                                    <thead>
                                        <tr>
                                            <th style="width:50%">Deksripsi</th>
                                            <th style="width:8%" class="text-center">Unit</th>
                                            <th style="width:8%" class="text-center">Hari</th>
                                            <th style="width:17%" class="text-center">Harga Satuan</th>
                                            <th style="width:17%" class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $gtotal = 0 @endphp
                                        @foreach($guards as $_guard)
                                        @php $gtotal = $gtotal+$_guard->qty @endphp
                                        <tr>
                                            <td>{!! $_guard->job !!}</td>
                                            <td class="text-center">{!!$_guard->qty !!}</td>
                                            <td class="text-center">{!!$_guard->day !!}</td>
                                            <td class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($_guard->price)}}
                                            </td>
                                            <td class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($_guard->total)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <table class="table table-bordered" id="tblguardSum">
                                    <tbody>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                TOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->guards_total)}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-default col-md-12">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">Others</div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tblCart">
                                    <thead>
                                        <tr>
                                            <th style="width:50%">Deksripsi</th>
                                            <th style="width:8%" class="text-center">Unit</th>
                                            <th style="width:17%" class="text-center">Harga Satuan</th>
                                            <th style="width:17%" class="text-center">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($others as $_other)
                                        <tr>
                                            <td>{!! $_other->name !!}</td>
                                            <td class="text-center">{!!$_other->qty !!}</td>
                                            <td class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($_other->price)}}
                                            </td>
                                            <td class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($_other->total)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <table class="table table-bordered" id="tblguardSum">
                                    <tbody>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                TOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->others_total)}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-default col-md-12">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">Summary</div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered " id="tblGuardsSum">
                                    <tbody>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                ITEMS TOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->cart_total)}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                GUARDS TOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->guards_total)}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                OTHERS TOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->others_total)}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                TAX PPN (10%)
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->tax_total)}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right font-weight-bold" style="width:83%">
                                                GRAND TOTAL
                                            </td>
                                            <td class="text-right" style="width: 17%">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($transaction->total)}}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="payment">
                <div class="row">
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-defaul">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">Payments</div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td style="width:10%">Total</td>
                                            <td>:</td>
                                            <td style="width:90%" class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{ number_format($transaction->total) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%">Paid</td>
                                            <td>:</td>
                                            <td style="width:90%" class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{ number_format($payments->amounts) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%">Outstanding</td>
                                            <td>:</td>
                                            <td style="width:90%" class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{ number_format($transaction->total-$payments->amounts) }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-primary">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold"></div>
                                <div class="card-title pull-right font-weight-bold">
                                    <a class="btn btn-info btn-sm text-light" id="btnCustFind" data-toggle="modal" data-target="#modalAddPayments">Add</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Seq.</th>
                                            <th>Date</th>
                                            <th>Received From</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Desc</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $payment_list = json_decode($payments->list,TRUE);
                                        @endphp
                                        @forelse($payment_list as $_payment)
                                        <tr>
                                            <td class="text-left">
                                                @if($_payment['sequence'] == 0)
                                                    Down Payment
                                                @else
                                                    {{$_payment['sequence']}}
                                                @endif
                                            </td>
                                            <td>{{$_payment['date']}}</td>
                                            <td>{{$_payment['name']}}</td>
                                            <td>{{($_payment['type'] == 1) ? 'Cash' : 'Transfer' }}</td>
                                            <td class="text-right">
                                                <span class="pull-left">Rp.</span>
                                                {{number_format($_payment['amount'])}}
                                            </td>
                                            <td>{{$_payment['desc']}}</td>
                                        </tr>
                                        @empty

                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="inventory">
                <div class="row">
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-default">
                            <div class="card-header  ">
                                <div class="card-title font-weight-bold">IN/OUT ITEMS</div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tblCart">
                                    <thead>
                                        <tr>
                                            <th style="">Code</th>
                                            <th style="">Date</th>
                                            <th style="">Out/In</th>
                                            <th style="">Giver</th>
                                            <th style="">Receiver</th>
                                            <th style="">Items</th>
                                            <th style="">PDF</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($inout as $_inout)
                                        @php
                                            $items = json_decode($_inout->items,true);
                                        @endphp
                                        <tr>
                                            <td>{{ $_inout->number }}</td>
                                            <td>{{ $_inout->created_at }}</td>
                                            <td class="text-center">{{ ($_inout->type == 1) ? 'OUT' : 'IN' }}</td>
                                            <td class="text-center">{{ $_inout->given_by }}</td>
                                            <td class="text-center">{{ $_inout->receiver_by }}</td>

                                            <td class="text-center">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        @foreach($items as $_is)
                                                        <tr>
                                                            <td style="width: 60%">
                                                                {{ $_is['product_name'] }}
                                                            </td>
                                                            <td style="width: 20%">
                                                                {{ $_is['barcode'] }}
                                                            </td>
                                                            <td style="width: 20%">
                                                                {{ $_is['serial_no'] }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <a href="{{'/inventories/inout/unduh/'.$transaction->id.'/'.$_inout->id}}" class="btn btn-info btn-sm text-light">
                                                    PDF
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="guard">
                <div class="row">
                    <div class="col-md-6">
                        <div id="card-linear-color" class="card card-default">
                            <div class="card-header  ">
                                <div class="card-title col-12 font-weight-bold">
                                    <div class="col-6">
                                        Check In
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <img src="data:image/jpg;base64,{{ $transaction->check_in_pic }}" class="img-resposive" alt="">
                                <br>
                                {{ $transaction->check_in_at }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="card-linear-color" class="card card-default">
                            <div class="card-header  ">
                                <div class="card-title col-12 font-weight-bold">
                                    <div class="col-6">
                                        Check Out
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <img src="data:image/jpg;base64,{{ $transaction->check_out_pic }}" class="img-resposive" alt="">
                                <br>
                                {{ $transaction->check_out_at }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="card-linear-color" class="card card-default">
                            <div class="card-header  ">
                                <div class="card-title col-12 font-weight-bold">
                                    <div class="col-6">
                                        Guard
                                    </div>
                                    <div class="col-6 pull-right">
                                        <button type="button" class="btn btn-default btn-cons pull-right" data-toggle="modal" data-target="#modalUpdatePicGuard">
                                            Pick PIC
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered" id="tblCart">
                                    <thead>
                                        <tr>
                                            <th style="">#</th>
                                            <th style="">Job</th>
                                            <th style="">Name</th>
                                            <th style="">Phone</th>
                                            <th style="">Fee</th>
                                            <th style="">PIC</th>
                                            {{-- <th style="">Action</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php $no=1 @endphp
                                    @foreach($guards as $_guard)
                                        <tr>
                                            <tr>
                                                <td>{{ $no }}</td>
                                                <td>{{ $_guard->job }}</td>
                                                <td>{{ $_guard->name }}</td>
                                                <td>{{ $_guard->phone }}</td>
                                                <td>Rp. {{ number_format($_guard->rates) }}</td>
                                                <td>{{ ($_guard->is_pic == 1) ? 'Yes' : '' }}</td>
                                                {{-- <td>
                                                    <form action="{{url('transactions/' . $transaction->id)}}" enctype="multipart/form-data" method="POST" role="form">
                                                        @csrf
                                                        <input type="text" name="transaction_id" value="{{$transaction->id}}" class="hidden">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_type" value="delete_guard">
                                                        <input type="hidden" name="guard_id" value="{{ $_guard->id }}">
                                                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                                                    </form>
                                                </td> --}}
                                            </tr>
                                        </tr>
                                        @php $no++ @endphp
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalUpdateStatus" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Update Status</h5>
            </div>
            <div class="modal-body">
                <br>
                <form action="{{ url('transactions/' . $transaction->id) }}" enctype="multipart/form-data" method="POST" role="form">
                    @csrf
                    <input type="text" name="transaction_id" value="{{ $transaction->id }}" class="hidden">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_type" value="update_status">
                    <div class="form-group form-group-default required">
                        <label for="">Status</label>
                        <select class="full-width" name="new_status" id="selectStatus" data-init-plugin="select2">
                            @foreach($_statuses as $_status)
                            <option value="{{ $_status->code }}" {{ ($transaction->status == $_status->code) ? 'selected' : '' }} >{{ $_status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group form-group form-group-default" id='divDateTimeDone'>
                        <label for="full_name">Done Datetime</label>
                        <input type="datetime-local" class="form-control" name="done_time" id="inputDateTimeDone" min="{{ $transaction->end_date }}T23:59">
                    </div>
                    <div class="form-group form-group form-group-default" id='divDateTimeDoneEquips'>
                        <label for="full_name">Over Time Equipments (<span id="spanOverTimeEquips"></span>)</label>
                        <input type="number" class="form-control" name="over_time_equips" id="inputOverTimeEquips" min="0">
                    </div>
                    <div class="form-group form-group form-group-default" id='divDateTimeDoneGuards'>
                        <label for="full_name">Over Time Guards (<span id="spanOverTimeGuards"></span>)</label>
                        <input type="number" class="form-control" name="over_time_guards" id="inputOverTimeGuards" min="0">
                    </div>

                    {{-- <div class="form-group form-group form-group-default required">
                        <label for="full_name">Note</label>
                        <textarea class="form-control" name="new_note_status"></textarea>
                    </div> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalUpdatePaymentStatus" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Update Payment Status</h5>
            </div>
            <div class="modal-body">
                <br>
                <form action="{{ url('transactions/' . $transaction->id) }}" enctype="multipart/form-data" method="POST" role="form">
                    @csrf
                    <input type="text" name="transaction_id" value="{{ $transaction->id }}" class="hidden">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_type" value="update_status_payment">
                    <div class="form-group form-group-default required">
                        <label for="">Status</label>
                        <select class="full-width" name="new_status" data-init-plugin="select2">
                            @foreach($_payment_statuses as $_status)
                            <option value="{{ $_status->code }}" {{ ($transaction->payment_status == $_status->code) ? 'selected' : '' }} >{{ $_status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="form-group form-group form-group-default required">
                        <label for="full_name">Note</label>
                        <textarea class="form-control" name="new_note_status"></textarea>
                    </div> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalAddPayments" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Add New Payment</h5>
            </div>
            <div class="modal-body">
                <br>
                <form action="{{url('transactions/' . $transaction->id)}}" enctype="multipart/form-data" method="POST" role="form">
                    @csrf
                    <input type="text" name="transaction_id" value="{{$transaction->id}}" class="hidden">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_type" value="add_payment">
                    <div class="form-group form-group-default required">
                        <label for="">Date</label>
                        <input type="date" name="date" value="{{ date('Y-m-d') }}" class="form-control" id="" >
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="">Received From</label>
                        <input type="text" name="name" class="form-control" id="" >
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="">Amount</label>
                        <input type="number" min="1" max="{{$transaction->total}}" name="amount" class="form-control" id="" >
                    </div>
                    <div class="form-group form-group form-group-default required">
                        <label for="full_name">Type</label>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio1" value="1" checked>
                            <label for="radio1">Cash</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio2" value="2">
                            <label for="radio2">Transfer</label>
                        </div>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Desc / Note</label>
                        <textarea name="desc" id="address" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">File</label>
                        <input type="file" name="file"  class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalUpdatePicGuard" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Pick Pic Guard</h5>
            </div>
            <div class="modal-body">
                <br>
                <form action="{{ url('transactions/' . $transaction->id) }}" enctype="multipart/form-data" method="POST" role="form">
                    @csrf
                    <input type="text" name="transaction_id" value="{{ $transaction->id }}" class="hidden">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_type" value="update_pic_guard">
                    <div class="form-group form-group-default required">
                        <label for="">Status</label>
                        <select class="full-width" name="pic" data-init-plugin="select2">
                            @foreach($guards as $_guard)
                            <option value="{{ $_guard->id }}" {{ ($_guard->is_pic == 1) ? 'selected' : '' }} >{{ $_guard->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="form-group form-group form-group-default required">
                        <label for="full_name">Note</label>
                        <textarea class="form-control" name="new_note_status"></textarea>
                    </div> --}}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{-- <div class="modal right fade" id="modalAddOperator" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Assign New Operator</h5>
            </div>
            <div class="modal-body">
                <br>
                <form action="{{url('transactions/' . $transaction->id)}}" enctype="multipart/form-data" method="POST" role="form">
                    @csrf
                    <input type="text" name="transaction_id" value="{{$transaction->id}}" class="hidden">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_type" value="add_operator">

                    <table class="table table-bordered" id="tblCart">
                        <thead>
                            <tr>
                                <th style="">#</th>
                                <th style="">Name</th>
                                <th style="">Phone</th>
                                <th style="">Fee</th>
                                <th style="">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($operator_list as $_list)
                            <tr>
                                <td>{{ $_list->name }}</td>
                                <td>{{ $_list->desc }}</td>
                                <td>{{ $_list->phone }}</td>
                                <td>{{ number_format($_list->fee) }}</td>
                                <td><input type="checkbox" class="select-guard" name="operator_pick[]" id="" value="{{  json_encode($_list) }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
                </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal --> --}}


<style>
input[readonly] {
    background-color: #e5ebf9 !important;
    color:black !important;
}
</style>

<script>
    $(document).ready(function() {
        $('#divDateTimeDone').hide()
        $('#divDateTimeDoneEquips').hide()
        $('#divDateTimeDoneGuards').hide()

        var limitGuard= {{ $gtotal }}
        $('input.select-guard').on('change', function(evt) {
            if($('input.select-guard:checked').length > limitGuard) {
                $('input.select-guard:not(:checked)').attr('disabled', 'disabled');
                this.checked = false;
                alert('Max 2 Operator pick')
            }
            else {
                console.log($('input.select-guard:checked').length)
            }
        });
        $('#selectStatus').change(function(){
            var i = $( "#selectStatus option:selected" ).val()
            if(i == '2') {
                $('#divDateTimeDone').show();
            }
            else {
                $('#divDateTimeDone').hide();
            }
        })

        $('#inputDateTimeDone').change(function(){
            var t = $(this).val()
            var diff = moment(t).diff('{{ $transaction->end_date }}T24:00','minutes')
            console.log(diff)

            var totalEquiep = 0
            var carts = <?= json_encode($carts)?>;
            $.each(carts, function (key, val) {
                totalEquiep = totalEquiep+(val.qty*val.price)
            });

            var totalGuards = 0
            var guards = <?= json_encode($guards)?>;
            $.each(guards, function (key, val) {
                totalGuards = totalGuards+(val.qty*val.price)
            });

            var oTE = 0
            var oTG = 0
            if(diff > 0 && diff <= 180) {
                if(diff >= 60) oTE = totalEquiep/100*50;

                oTG = totalGuards/100*50;
            }
            else if(diff > 180){
                oTE = totalEquiep;
                oTG = totalGuards;
            }
            $('#divDateTimeDoneEquips').show();
            $('#spanOverTimeEquips').html((diff/60).toFixed(2)+' Hours')
            $('#inputOverTimeEquips').val(oTE)

            $('#divDateTimeDoneGuards').show();
            $('#spanOverTimeGuards').html((diff/60).toFixed(2)+' Hours')
            $('#inputOverTimeGuards').val(oTG)

        })
    })
</script>

@endsection
