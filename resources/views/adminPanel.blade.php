@extends('layouts.adminLayout')

@section('title')
    Dashboard
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item active">Dashboard</li>
@endsection

@section('head')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <p><em>resources/views/adminPanel.blade.php</em></p>
    <div class="row">
        <div class="col-sm-9">
            <div class="row mb-3">
                <div class="col-sm-12">
                    <div class="widget card bg-white no-margin widget-loader-bar">
                        <div class="card-header top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Overview</span>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="row">
                                <div class="col-sm-auto col-12" style="min-width: 20%">
                                    <div class="widget card bg-warning widget-loader-bar mb-0">
                                        <div class="card-body p-3">
                                            <h2 class="no-margin p-b-5 semi-bold">5</h2>
                                            <p class="mb-0">Rent Due Today</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-auto col-12" style="min-width: 20%">
                                    <div class="widget card bg-warning widget-loader-bar mb-0">
                                        <div class="card-body p-3">
                                            <h2 class="no-margin p-b-5 semi-bold">6</h2>
                                            <p class="mb-0">Rent Overdue</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-auto col-12" style="min-width: 20%">
                                    <div class="widget card bg-warning widget-loader-bar mb-0">
                                        <div class="card-body p-3">
                                            <h2 class="no-margin p-b-5 semi-bold">7</h2>
                                            <p class="mb-0">Item In</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-auto col-12" style="min-width: 20%">
                                    <div class="widget card bg-warning widget-loader-bar mb-0">
                                        <div class="card-body p-3">
                                            <h2 class="no-margin p-b-5 semi-bold">8</h2>
                                            <p class="mb-0">Item Out</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-auto col-12" style="min-width: 20%">
                                    <div class="widget card card-danger widget-loader-bar mb-0">
                                        <div class="card-body p-3">
                                            <h2 class="no-margin p-b-5 semi-bold">9</h2>
                                            <p class="mb-0">Expiring</p>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="col">
                                    <div class="widget card card-danger widget-loader-bar mb-0">
                                        <div class="card-body p-3">
                                            <h2 class="no-margin p-b-5 semi-bold">0</h2>
                                            <p class="mb-0">Cancel Order</p>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="row">
                <div class="col-sm-12 mb-3">
                    <div class="widget card bg-white no-margin widget-loader-bar">
                        <div class="card-header top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Rents</span>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="row">
                                <div class="col-sm-12">
                                    <figure class="highcharts-figure">
                                        <div id="container-speed" class="chart-container"></div>
                                    </figure>
                                </div>
                                <div class="col-sm-12 mb-3">
                                    <p class="mb-0 text-uppercase"><small>Due This Month</small></p>
                                    <h4 class="mb-0 text-left">Rp230.000.000</h4>
                                </div>
                                <div class="col-sm-12">
                                    <p class="mb-0 text-uppercase"><small>Receive This Month</small></p>
                                    <h4 class="mb-0 text-left">Rp184.000.000</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="widget card bg-white no-margin widget-loader-bar">
                        <div class="card-header top-left top-right ">
                            <div class="card-title text-black hint-text">
                                <span class="font-montserrat fs-11 all-caps">Items</span>
                            </div>
                        </div>
                        <div class="card-body p-t-40">
                            <div class="row">
                                <div class="col-sm-12">
                                    <figure class="highcharts-figure">
                                        <div id="container-speed" class="chart-container"></div>
                                    </figure>
                                </div>
                                <div class="col-sm-12 mb-3">
                                    <p class="mb-0 text-uppercase"><small>Total Items</small></p>
                                    <h4 class="mb-0 text-left">230</h4>
                                </div>
                                <div class="col-sm-12">
                                    <p class="mb-0 text-uppercase"><small>Out Items</small></p>
                                    <h4 class="mb-0 text-left">184</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- <script src="{{ asset('js/pages.js')}}"></script> --}}
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script>
        var gaugeOptions = {
            chart: {
                type: 'solidgauge'
            },

            title: null,

            pane: {
                center: ['50%', '85%'],
                size: '160%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#f4f4f4',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            tooltip: {
                enabled: false
            },

            // the value axis
            yAxis: {
                stops: [
                    // [0.1, '#d83c31'], // green
                    // [0.5, '#ffd945'], // yellow
                    // [0.9, '#26bf93']  // red

                    [0.1, '#0bb8aa'],
                    [0.5, '#0bb8aa'],
                    [0.9, '#0bb8aa']
                ],
                lineWidth: 0,
                tickWidth: 0,
                minorTickInterval: null,
                tickAmount: 0,
                labels: {
                    y: 16
                },
                left: 0,
            },

            xAxis: {
                left: 0,
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };

        // The speed gauge
        var chartSpeed = Highcharts.chart('container-speed', Highcharts.merge(gaugeOptions, {
            yAxis: {
                min: 0,
                max: 100,
            },

            credits: {
                enabled: false
            },

            series: [{
                name: 'Speed',
                data: [80],
                dataLabels: {
                    y: -20,
                    format:
                        '<div style="text-align:center">' +
                        '<span style="font-size:12px" class="font-inter">{y}</span>' +
                        '<span style="font-size:12px;" class="font-inter">% Received</span>' +
                        '</div>'
                },
            }]

        }));
    </script>
@endsection
