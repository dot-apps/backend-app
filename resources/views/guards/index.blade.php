@extends('layouts.adminLayout')

@section('title')
Operator List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Guards</a></li>
    <li class="breadcrumb-item active">Guards List</li>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th class="text-nowrap text-center" scope="col">#</th>
                                <th class="text-nowrap" scope="col">Name</th>
                                <th class="text-nowrap" scope="col">Job</th>
                                <th class="text-nowrap" scope="col">Phone</th>
                                <th class="text-nowrap" scope="col">Email</th>
                                <th class="text-nowrap" scope="col">Fee</th>
                                <th class="text-nowrap text-center" scope="col">Active</th>
                                <th class="text-nowrap text-center" scope="col">Available</th>
                                <th class="text-center" scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $no = 1 @endphp
                        @forelse ($guards as $guard)
                            <tr style="backgroundColor:#fff">
                                <td class="text-nowrap text-center">{{$no++}}</td>
                                <td class="text-nowrap">{{$guard->name}}</td>
                                <td class="text-nowrap">{{$guard->job}}</td>
                                <td class="text-nowrap">{{$guard->phone}}</td>
                                <td class="text-nowrap">{{$guard->email}}</td>
                                <td class="text-nowrap">Rp. {{number_format($guard->rates)}}</td>
                                <td class="text-nowrap text-center">{{$guard->is_active == 1 ? "Yes" : "No"}}</td>
                                <td class="text-nowrap text-center">{{$guard->is_avail == 1 ? "Yes" : "No"}}</td>
                                <td class="text-nowrap text-center justify-content-center">
                                    <a href="{{'guards/'.$guard->id}}" class="btn btn-info btn-sm text-light mr-2">View</a>
                                    <a href="{{'guards/'.$guard->id.'/edit'}}" class="btn btn-warning btn-sm mr-2">Edit</a>
                                    <form action="{{url('guards/'.$guard->id)}}" method="POST" style="display:inline-block">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <div class="display-3 text-center">No guards Available</div>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('table').DataTable();
    </script>
@endsection
