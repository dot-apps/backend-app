@extends('layouts.adminLayout')

@section('title')
Detail guard - {{$guard->name}}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="{{ url('/guards' )}}">Guards</a></li>
    <li class="breadcrumb-item active">Opertator Detail</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Guard Info</div>
            </div>
            <div class="card-body pb-2">
                <div class="row">
                    <div class="col-6">
                        <p class="text-label"><small>Name</small></p>
                        <p class="font-weight-bold">{{ $guard->name }}</p>
                    </div>
                    <div class="col-6">
                        <p class="text-label"><small>Rates</small></p>
                        <p class="font-weight-bold"><span class="pull-left">Rp. </span>
                                {{ number_format($guard->rates) }} / day</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p class="text-label"><small>Dob</small></p>
                        <p class="font-weight-bold">{{ $guard->dob }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <p class="text-label"><small>Gender</small></p>
                        <p class="font-weight-bold">{{ ($guard->gender == 'M') ? 'Male' : 'Female' }}</p>
                    </div>
                </div>
            </div>
            <div class="card-header  border-top">
                <div class="card-title">Guard Contact</div>
            </div>
            <div class="card-body pb-2">
                <div class="row">
                    <div class="col-6">
                        <p class="text-label"><small>Phone</small></p>
                        <p class="font-weight-bold">{{ $guard->phone }}</p>
                    </div>
                    <div class="col-6">
                        <p class="text-label"><small>Email</small></p>
                        <p class="font-weight-bold">{{ $guard->email }}</p>
                    </div>
                </div>
            </div>
            <div class="card-header border-top">
                <div class="card-title">Guard Status</div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <p class="text-label"><small>Status</small></p>
                        <p class="font-weight-bold mb-0">{{$guard->is_active == 1 ? 'ACTIVE' : 'UN-ACTIVE' }}</p>
                    </div>
                    <div class="col-6">
                        <p class="text-label"><small>Availability</small></p>
                        <p class="font-weight-bold mb-0">{{$guard->is_avail == 1 ? 'AVAILABLE' : 'UN-AVAILABLE' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Guard Image</div>
            </div>
            <div class="card-body pb-0">
                <div class="row">
                    <div class="col-sm-6 mb-4">
                        <p class="text-label mb-2"><small>Photo</small></p>
                        <img class="img-thumbnail img-fluid" src="{{($guard->pic == NULL) ? '/img/no-photo.png' : '/storage/' . $guard->pic }}" alt="image">
                    </div>
                    <div class="col-sm-6 mb-4">
                        <p class="text-label mb-2"><small>KTP</small></p>
                        <img class="img-thumbnail img-fluid text-center" src="{{($guard->ktp == NULL) ? '/img/no-photo.png' : '/storage/' . $guard->ktp }}" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
