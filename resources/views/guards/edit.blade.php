@extends('layouts.adminLayout')

@section('title')
Edit guard - {{ $guard->name }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="{{ url('/guards' )}}">Guards</a></li>
    <li class="breadcrumb-item active">Edit guard</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Edit new guard</div>
            </div>
            <form method="POST" action="{{url('guards/' . $guard->id)}}" enctype="multipart/form-data">
            @csrf
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group form-group-default required">
                        <label for="full_name">Job</label>
                        <input type="text" name="job" class="form-control" list="job" value="{{ $guard->job }}" />
                        <datalist id="job">
                        @forelse ($guards as $_guard)
                            <option>{{ $_guard->job}}</option>
                        @empty
                        @endforelse
                        </datalist>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Full Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ $guard->name }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ $guard->email }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="pass">phone</label>
                        <input type="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" id="pass" value="{{ $guard->phone }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="pass">Gender</label>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="male" name="gender" value="M" class="custom-control-input" {{ ($guard->gender == 'M') ? 'checked' : '' }}/>
                            <label class="custom-control-label" for="male">Male</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="female" name="gender" value="F" class="custom-control-input" {{ ($guard->gender == 'F') ? 'checked' : '' }} />
                            <label class="custom-control-label" for="female">Female</label>
                        </div>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="dob">Birth Date</label>
                        <input type="date" name="dob" class="form-control @error('dob') is-invalid @enderror" value="{{ $guard->dob }}" id="dob" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="dob">Rate</label>
                        <input type="number" name="rates" min="0" class="form-control @error('rates') is-invalid @enderror" value="{{ $guard->rates }}" id="dob" required>
                    </div>
                    <div class="form-group form-group-default ">
                        <label for="ktp">Upload KTP</label>
                        <img class="img-thumbnail" src="<?= asset("storage/$guard->ktp")?>" alt="">
                        <input type="file" class="form-control @error('ktp') is-invalid @enderror" id="ktp" value="{{ old('ktp') }}" name="ktp" >
                    </div>
                    <div class="form-group form-group-default mb-0">
                        <label for="pic">Upload Photo</label>
                        <img class="img-thumbnail" src="<?= asset("storage/$guard->pic")?>" alt="">
                        <input type="file" class="form-control @error('pic') is-invalid @enderror" id="pic" value="{{ old('pic') }}" name="pic" >
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="_method" value="PUT">
                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
