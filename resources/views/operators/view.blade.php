@extends('layouts.adminLayout')

@section('title')
Detail Operator - {{$operator->name}}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/operators">Operators</a></li>
    <li class="breadcrumb-item active">Opertator Detail</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Info</div>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>{{ $operator->name }}</th>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <th>{{ $operator->phone }}</th>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <th>{{ $operator->email }}</th>
                        </tr>
                        <tr>
                            <th>DOB</th>
                            <th>{{ $operator->dob }}</th>
                        </tr>
                        <tr>
                            <th>Fee</th>
                            <th>
                                <span class="pull-left">Rp. </span>
                                {{ number_format($operator->fee) }}
                                / day
                            </th>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <th>{{$operator->is_active == 1 ? 'ACTIVE' : 'UN-ACTIVE' }}</th>
                        </tr>
                        <tr>
                            <th>Availability</th>
                            <th>{{$operator->is_avail == 1 ? 'AVAILABLE' : 'UN-AVAILABLE' }}</th>
                        </tr>
                        
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Info</div>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th style="vertical-align: top">Photo</th>
                            <th>
                                <img class="img-thumbnail" src="{{($operator->pic == NULL) ? '/img/no-photo.png' : '/storage/' . $operator->pic }}" alt="image">
                            </th>
                        </tr>
                        <tr>
                            <th style="vertical-align: top">KTP</th>
                            <th>
                                <img class="img-thumbnail text-center" src="{{($operator->ktp == NULL) ? '/img/no-photo.png' : '/storage/' . $operator->ktp }}" alt="image">
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div> 
</div>
@endsection