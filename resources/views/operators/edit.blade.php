@extends('layouts.adminLayout')

@section('title')
Edit Operator - {{ $operator->name }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/operators">Operators</a></li>
    <li class="breadcrumb-item active">Edit Operator</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add new operator</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{url('operators/' . $operator->id)}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group form-group-default required">
                        <label for="full_name">Full Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ $operator->name }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ $operator->email }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="pass">phone</label>
                        <input type="phone" name="phone" class="form-control @error('phone') is-invalid @enderror" id="pass" value="{{ $operator->phone }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="dob">Birth Date</label>
                        <input type="date" name="dob" class="form-control @error('dob') is-invalid @enderror" value="{{ $operator->dob }}" id="dob" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="dob">Fee</label>
                        <input type="number" name="fee" min="0" class="form-control @error('dob') is-invalid @enderror" value="{{ $operator->fee }}" id="dob" required>
                    </div>
                    <div class="form-group form-group-default ">
                        <label for="ktp">Upload KTP</label>
                        <img class="img-thumbnail" src="<?= asset("storage/$operator->ktp")?>" alt="">
                        <input type="file" class="form-control @error('ktp') is-invalid @enderror" id="ktp" value="{{ old('ktp') }}" name="ktp" >
                    </div>
                    <div class="form-group form-group-default ">
                        <label for="pic">Upload Photo</label>
                        <img class="img-thumbnail" src="<?= asset("storage/$operator->pic")?>" alt="">
                        <input type="file" class="form-control @error('pic') is-invalid @enderror" id="pic" value="{{ old('pic') }}" name="pic" >
                    </div>
                    <input type="hidden" name="_method" value="PUT">
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection