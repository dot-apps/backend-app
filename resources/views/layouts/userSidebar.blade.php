<li class="{{ Request::segment(1) === 'products' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Alat</span>
        <span class="arrow {{ Request::segment(1) === 'products' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">list_bullets</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'products' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{url('/products/create')}}">Tambah Alat</a>
            <span class="icon-thumbnail"><i class="pg-icon">ta</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'products' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/products' )}}">Daftar Alat</a>
            <span class="icon-thumbnail"><i class="pg-icon">da</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'packages' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Paket</span>
        <span class="arrow {{ Request::segment(1) === 'packages' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">folder</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'packages' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{url('/packages/create')}}">Tambah Paket</a>
            <span class="icon-thumbnail"><i class="pg-icon">tp</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'packages' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/packages' )}}">Daftar Paket</a>
            <span class="icon-thumbnail"><i class="pg-icon">dp</i></span>
        </li>
    </ul>
</li>

{{-- <li class="{{ Request::segment(1) === 'operators' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Operators</span>
        <span class="arrow {{ Request::segment(1) === 'operators' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'operators' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/operators' )}}">Operator List</a>
            <span class="icon-thumbnail"><i class="pg-icon">ol</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'operators' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/operators/create'}}">New Operator</a>
            <span class="icon-thumbnail"><i class="pg-icon">no</i></span>
        </li>
    </ul>
</li> --}}

<li class="{{ Request::segment(1) === 'guards' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Guards</span>
        <span class="arrow {{ Request::segment(1) === 'guards' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'guards' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/guards' )}}">Guard List</a>
            <span class="icon-thumbnail"><i class="pg-icon">gl</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'guards' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{url('/guards/create')}}">New Guarrd</a>
            <span class="icon-thumbnail"><i class="pg-icon">ng</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'customers' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Clients</span>
        <span class="arrow {{ Request::segment(1) === 'customers' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'customers' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/customers' )}}">Client List</a>
            <span class="icon-thumbnail"><i class="pg-icon">cl</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'customers' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{url('/customers/create')}}">New Client</a>
            <span class="icon-thumbnail"><i class="pg-icon">nc</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'partners' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Vendors</span>
        <span class="arrow {{ Request::segment(1) === 'partners' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'partners' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/partners' )}}">Vendor List</a>
            <span class="icon-thumbnail"><i class="pg-icon">vl</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'partners' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{url('/partners/create')}}">New Vendor</a>
            <span class="icon-thumbnail"><i class="pg-icon">nv</i></span>
        </li>
    </ul>
</li>

<li class="{{ Request::segment(1) === 'inventories' || Request::segment(1) === 'inventories/track' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Inventories</span>
        <span class="arrow {{ Request::segment(1) === 'invetories' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">camera</i></span>
    <ul class="sub-menu">
        <li class="{{ (Request::segment(1) === 'inventories' && Request::segment(2) != 'partner' && Request::segment(2) != 'inout' ) ? 'active' : null }}">
            <a  href="{{ url('inventories' )}}">List Inventory</a>
            <span class="icon-thumbnail"><i class="pg-icon">li</i></span>
        </li>
        <li class="{{ Request::segment(2) === 'partner' ? 'active' : null }}">
            <a  href="{{ url('inventories/partner' )}}">Partners</a>
            <span class="icon-thumbnail"><i class="pg-icon">pa</i></span>
        </li>
        <li class="{{ Request::segment(2) === 'inout' ? 'active' : null }}">
            <a  href="{{ url('inventories/inout' )}}">In Out</a>
            <span class="icon-thumbnail"><i class="pg-icon">io</i></span>
        </li>
        {{-- <li class="{{ Request::segment(2) === 'track' ? 'active' : null }}">
            <a  href="{{ url('inventories/track' )}}">Track Inventory</a>
            <span class="icon-thumbnail"><i class="pg-icon">ti</i></span>
        </li> --}}
    </ul>
</li>

{{-- <li class="{{ Request::segment(1) === 'quotations' ? ' open active' : null }}">
    <a href="javascript:void(0);"><span class="title">Quotations</span>
        <span class="arrow {{ Request::segment(1) === 'quotations' ? ' open active' : null }}"></span></a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'quotations' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/quotations' )}}">Quotations List</a>
            <span class="icon-thumbnail"><i class="pg-icon">ol</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'quotations' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{'/quotations/create'}}">New Quotation</a>
            <span class="icon-thumbnail"><i class="pg-icon">no</i></span>
        </li>
    </ul>
</li> --}}

<li class="{{ Request::segment(1) === 'transactions' ? ' open active' : null }}">
    <a href="javascript:void(0);">
        <span class="title">Transactions</span>
        <span class="arrow {{ Request::segment(1) === 'transactions' ? ' open active' : null }}"></span>
    </a>
    <span class="icon-thumbnail"><i class="pg-icon">user</i></span>
    <ul class="sub-menu">
        <li class="{{ Request::segment(1) === 'transactions' && Request::segment(2) === 'create' ? ' active' : null }}">
            <a href="{{url('/transactions/create')}}">New Transaction</a>
            <span class="icon-thumbnail"><i class="pg-icon">nt</i></span>
        </li>
        <li class="{{ Request::segment(1) === 'transactions' && Request::segment(2) === null ? ' active' : null }}">
            <a href="{{ url('/transactions' )}}">Transactions List</a>
            <span class="icon-thumbnail"><i class="pg-icon">tl</i></span>
        </li>
    </ul>
</li>
