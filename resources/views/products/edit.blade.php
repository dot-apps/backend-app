@extends('layouts.adminLayout')

@section('title')
Edit Procuct - {{$product->name}}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="{{ url('/products' )}}">Alat</a></li>
    <li class="breadcrumb-item active">Ubah Alat</li>
@endsection

@section('content')
<div class="row">
<div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Ubah Alat</div>
            </div>
            <form method="POST" action="{{url('products/' . $product->id)}}" enctype="multipart/form-data">
            @csrf
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group form-group-default required">
                        <label for="full_name">Kategori</label>
                        <input type="text" name="category" class="form-control" list="categories" value="{{$product->category}}" />
                        <datalist id="categories">
                        @forelse ($categories as $category)
                            <option>{{ $category->category}}</option>
                        @empty
                        @endforelse
                        </datalist>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Merek</label>
                        <input type="text" name="brand" class="form-control" list="brands" value="{{$product->brand}}" />
                        <datalist id="brands">
                        @forelse ($brands as $brand)
                            <option>{{ $brand->brand}}</option>
                        @empty
                        @endforelse
                        </datalist>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Nama</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ $product->name }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Harga 1 Hari</label>
                        <input type="number" name="price_day" min=1000 class="form-control @error('price_day') is-invalid @enderror" id="price_day" value="{{ $product->price_day }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Harga 7 Hari</label>
                        <input type="number" name="price_7days" min=1000 class="form-control @error('price_7days') is-invalid @enderror" id="price_7days" value="{{ $product->price_7days }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="desc">Keterangan</label>
                        <textarea name="desc" id="desc" class="form-control" cols="30" rows="10">{{ $product->desc }}</textarea>
                    </div>
                    <div class="form-group form-group-default mb-0">
                        <label for="photo">Upload Photo</label>
                        <img class="img-thumbnail text-center" src="{{($product->photo == NULL) ? '/img/no-photo.png' : '/storage/' . $product->photo }}" alt="image">
                        <br/>
                        <br/>
                        <input type="file" class="form-control @error('photo') is-invalid @enderror" id="photo" value="{{ old('photo') }}" name="photo">
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-3 mr-4">
                            <a href="{{ url('products')}}" class="btn btn-secondary btn-block">Batal</a>
                        </div>
                        <div class="col-3">
                            <input type="hidden" name="_method" value="PUT">
                            <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
