@extends('layouts.adminLayout')

@section('title')
Product List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Products</a></li>
    <li class="breadcrumb-item active">Products List</li>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th class="text-nowrap text-center" scope="col">#</th>
                                <th class="text-nowrap" scope="col">Category</th>
                                <th class="text-nowrap" scope="col">Brand</th>
                                <th class="text-nowrap w-full" scope="col">Name</th>
                                <th class="text-nowrap" scope="col">Description</th>
                                <th class="text-nowrap" scope="col">Price per Day</th>
                                <th class="text-nowrap" scope="col">Price 7 Days</th>
                                <th class="text-nowrap text-center" scope="col">Active</th>
                                <th class="text-nowrap text-center" scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $no = 1 @endphp
                        @forelse ($products as $product)
                            <tr style="backgroundColor:#fff">
                                <td class="text-nowrap text-center">{{$no++}}</td>
                                <td class="text-nowrap">{{$product->category}}</td>
                                <td class="text-nowrap">{{$product->brand}}</td>
                                <td class="text-nowrap">{{$product->name}}</td>
                                <td class="text-nowrap">{{$product->desc}}</td>
                                <td class="text-nowrap">Rp. {{number_format($product->price_day)}}</td>
                                <td class="text-nowrap">Rp. {{number_format($product->price_7days)}}</td>
                                <td class="text-nowrap text-center">{{$product->is_active == 1 ? "Yes" : "No"}}</td>
                                <td class="text-nowrap text-center justify-content-center">
                                    <a href="{{'products/'.$product->id}}" class="btn btn-info btn-sm text-light mr-2">View</a>
                                    <a href="{{'products/'.$product->id.'/edit'}}" class="btn btn-warning btn-sm mr-2">Edit</a>
                                    <form action="{{url('products/'.$product->id)}}" method="POST" style="display:inline-block">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <div class="display-3 text-center">No Products Available</div>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('table').DataTable();
    </script>
@endsection
