@extends('layouts.adminLayout')

@section('title')
Detail Package - {{ $package->name }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="{{ url('/packages' )}}">Packages</a></li>
    <li class="breadcrumb-item active">Package Detail</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-4">
        <div id="card-linear-color" class="card card-default">
            <div class="card-body">
                <h3 class="text-center mt-0 mb-4 text-capitalize">{{$package->name}}</h3>
                <div class="row">
                    <div class="col-12">
                        <p class="mb-0"><small>Price per Day</small></p>
                        <p class="font-weight-bold">Rp {{number_format($package->price_day)}}</p>
                    </div>
                    <div class="col-12">
                        <p class="mb-0"><small>Price 7 Days</small></p>
                        <p class="font-weight-bold">Rp {{number_format($package->price_7days)}}</p>
                    </div>
                    <div class="col-12">
                        <p class="mb-0"><small>Status</small></p>
                        <p class="font-weight-bold">{{$package->is_active == 1 ? 'ACTIVE' : 'UN-ACTIVE' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-md-2"></div> -->
    <div class=" col-md-8">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Products</div>
            </div>
            <div class="card-body">
                <table class="table ">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="w-full">Name</th>
                            <th class="text-right">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $no= 1?>
                    @forelse ($packageProducts as $pacProd)
                        <tr>
                            <td class="text-center">{{$no}}</td>
                            <td class="w-full">{{$pacProd->name}}</td>
                            <td class="text-right">{{$pacProd->qty}}</td>
                        </tr>
                        <?php $no++?>
                    @empty
                        <p>No Products Available</p>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
