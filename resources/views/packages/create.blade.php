@extends('layouts.adminLayout')

@section('title')
New Package
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/packages">Packages</a></li>
    <li class="breadcrumb-item active">New Package</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="/packages" class="row" enctype="multipart/form-data">
            @csrf
            <div class="col-sm-6">
                <div class="card" style="padding: 20px">
                    <div class="card-header pl-0 pt-0">
                        <div class="card-title">Detail Pacakge</div>
                    </div>
                    <div class="cord-body">
                        <div class="form-group form-group-default required">
                            <label for="full_name">Name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ old('name') }}" required>
                        </div>
                        <div class="form-group form-group-default required">
                            <label for="full_name">Price per Day</label>
                            <input type="number" name="price_day" min=1000 class="form-control text-right @error('price_day') is-invalid @enderror" id="price_day" value="{{ old('price_day') }}" required>
                        </div>
                        <div class="form-group form-group-default required mb-0">
                            <label for="full_name">Price 7 Days</label>
                            <input type="number" name="price_7days" min=1000 class="form-control text-right @error('price_7days') is-invalid @enderror" id="price_7day" value="{{ old('price_7day') }}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 mb-4">
                <div class="card" style="padding: 20px">
                    <div class="card-header pl-0 pt-0">
                        <div class="card-title">Product Include</div>
                    </div>
                    <div class="cord-body">
                        <div class="row">
                            <div class="col-10">
                                <select class="form-control" id="selectProduct">
                                <option value="0" selected disabled>Select Product</option>
                                @foreach($products as $product)
                                <option id="optionProd{{$product->id}}" value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="col-2" style="padding-left: 20px">
                                <a href="#" class="btn btn-outline-primary w-full" id="btnPick">Pick</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="tblPickProduct">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-1 col-3">
                        <button type="submit" class="btn btn-primary btn-block">Create</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script>
        $('select').select2();
        $('#btnPick').on('click',function(){
            var productName = $('#selectProduct option:selected').text()
            var productId = $('#selectProduct option:selected').val()

            if(productId !=0){
                var html = '<tr id="rowProd'+productId+'">'
                    html +='<td class="w-full">'
                    html +='<input name="products[]" type="text" value="'+productId+'" class="hidden">'+productName+''
                    html +='</td>'
                    html +='<td class="">'
                    html +='<input name="qty[]" type="number" class="w-full text-right" min="1" value="1">'
                    html +='</td>'
                    html +='<td class="">'
                    html +='<a class="btn btn-danger btn-sm text-light btnRemoveProduct" data-id="'+productId+'">Remove</a>'
                    html +='</td>'
                    html +='</tr>'
                $('#tblPickProduct > tbody:last-child').append(html)
                $('#optionProd'+productId).attr('disabled','disabled');
            }
        })
        $('tbody').on('click','.btnRemoveProduct',function(){
            var rowId = $(this).data('id')
            console.log(rowId)
            $('#rowProd'+rowId).remove()
            $('#optionProd'+rowId).removeAttr('disabled');
        })
    </script>
@endsection
