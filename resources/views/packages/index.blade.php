@extends('layouts.adminLayout')

@section('title')
Package List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Packages</a></li>
    <li class="breadcrumb-item active">Packages List</li>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th class="text-center" scope="col">#</th>
                                <th class="w-full" scope="col">Name</th>
                                <th class="text-nowrap" scope="col">Price per Day</th>
                                <th class="text-nowrap" scope="col">Price 7 Days</th>
                                <th class="text-nowrap text-center" scope="col">Active</th>
                                <th class="text-nowrap text-center" scope="col">Available</th>
                                <th class="text-center text-center" scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $no = 1 @endphp
                        @forelse ($packages as $package)
                            <tr style="backgroundColor:#fff">
                                <td class="text-center">{{$no++}}</td>
                                <td class="w-full">{{$package->name}}</td>
                                <td class="text-nowrap">Rp. {{number_format($package->price_day)}}</td>
                                <td class="text-nowrap">Rp. {{number_format($package->price_7days)}}</td>
                                <td class="text-nowrap text-center">{{$package->is_active == 1 ? "Yes" : "No"}}</td>
                                <td class="text-nowrap text-center">{{$package->is_avail == 1 ? "Yes" : "No"}}</td>
                                <td class="text-nowrap text-center justify-content-center">
                                    <a href="{{'packages/'.$package->id}}" class="btn btn-info btn-sm text-light mr-2">View</a>
                                    <a href="{{'packages/'.$package->id.'/edit'}}" class="btn btn-warning btn-sm mr-2">Edit</a>
                                    <form action="{{url('packages/'.$package->id)}}" method="POST" style="display:inline-block">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <div class="display-3 text-center">No packages Available</div>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('table').DataTable();
    </script>
@endsection
