<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		@if($transaction->type == 1)
		<title>PEMINJAMAN KE REKANAN</title>
		@else
		<title>PENGEMBALIAN KE REKANAN</title>
		@endif
	</head>
	<body style="font-family: 'Poppins', sans-serif; margin-top: 2cm; margin-bottom: 0.75cm; font-size: 11px;">
		<header style="position: fixed; top: -1.75cm; left: -1.25cm; right: -1.25cm; height: 1.14cm; width: 100%;">
			{{-- <img src="{{ asset('/img/letterhead/header.svg') }}" width="100%"> --}}
            <table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
                <tr style="align-items: center; vertical-align: middle">
                    <td style="width: 4cm; vertical-align: middle"><img src="{{ asset('/img/letterhead/logo.svg') }}" style="width: 90px; margin-left: 1.25cm; margin-top: 1cm"></td>
                    <td style="padding-right: 1.25cm; vertical-align: middle;">
                        <p class="text-footer" style="color: #2851a4; color: #2851a4; text-align: right; margin-bottom: 5px; font-size: 43px;">DIGITAL OPTIK TEKNOLOGI</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center; background: transparent;">
                        <p class="text-footer" style="color: #2851a4; color: #2851a4; margin-top: .25cm; text-align: center; margin-bottom: 0px; font-size: 12px;">Jl. Jeruk Raya &middot; Ruko Soho Jagakarsa No.9B Jakarta Selatan, 12620 &middot; 0857 1168 7748 &middot; hello@dot-rental.com &middot; @dot_rent</p>
                    </td>
                </tr>
            </table>
		</header>
        <hr style="margin-top: .6cm;">
		<main>
			<p style="text-align: center; margin-top: -.5cm; margin-bottom: 20px"><b>&nbsp;</b></p>
            @if($transaction->type == 1)
            <p style="text-align: center; margin-bottom: 20px; font-size: 20px; text-transform: uppercase;">tanda terima</p>
            @else
            <p style="text-align: center; margin-bottom: 20px; font-size: 20px; text-transform: uppercase;">tanda pengembalian</p>
            @endif
            <table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" border="1" width="100%">
                <thead>
                    <tr>
                        <th style="width: 5%; text-align: left;">NO</th>
                        <th style="width: 30%; text-align: left;">DESCRIPTION</th>
                        <th style="width: 5%; text-align: center;">QTY</th>
                        <th style="width: 20%; text-align: left;">SERIAL NO.</th>
                        <th style="width: 35%; text-align: left;">KETERANGAN</th>
                        <th style="width: 12%; text-align: left; white-space: nowrap;">TGL. KEMBALI</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 5%; text-align: left;">1</th>
                        <td style="width: 30%; text-align: left;">Nama item barang</th>
                        <td style="width: 5%; text-align: center;">12</th>
                        <td style="width: 20%; text-align: left;">KJSK109201KK</th>
                        <td style="width: 35%; text-align: left;">KETERANGAN</th>
                        <td style="width: 12%; text-align: left;"></th>
                        <!-- <td>1</td>
                        <td>NAMA ITEM</td>
                        <td>1</td>
                        <td>SNK80128910SOL</td>
                        <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam debitis voluptatem quam deserunt pariatur expedita impedit dolorem ab totam vero. Et illum possimus fugiat eius, cumque dignissimos blanditiis id nesciunt?</td> -->
                    </tr>
                </tbody>
            </table>
            <!-- kalau datanya sudah keluar ini tinggal di hapus -->
			<!-- <table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 10px;" width="100%">
				<tbody>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 18%; background-color: transparent; padding-top: 0; padding-bottom: 0; vertical-align: top;" width="18%" align="left" bgcolor="transparent">
							@if($transaction->type == 1)
							Tempat Sewa
							@else
							Telah diterima oleh
							@endif
						</td>
						<td style="padding: 5px; text-align: left; width: 1%; background-color: transparent; padding-top: 0; padding-bottom: 0; vertical-align: top;" width="1%" align="left" bgcolor="transparent">:</td>
						<td style="padding: 5px; text-align: left; width: 82%; background-color: transparent; padding-top: 0; padding-bottom: 0; vertical-align: top;" width="82%" align="left" bgcolor="transparent">
							{{ $partner->name }}
							<br>
							({{ $transaction->giver}})
						</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 18%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="18%" align="left" bgcolor="transparent">Alat/barang</td>
						<td style="padding: 5px; text-align: left; width: 1%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="1%" align="left" bgcolor="transparent">:</td>
						<td style="padding: 5px; text-align: left; width: 82%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="82%" align="left" bgcolor="transparent">
							@foreach(json_decode($transaction->products,TRUE) as $_prod)
                            {{ $_prod['qty'] }} x {{ $_prod['product_name'] }}
                            <br>
                            @endforeach
						</td>
					</tr>
					<tr>
						<td style="padding: 5px; padding-left: 0; text-align: left; width: 18%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="18%" align="left" bgcolor="transparent">Catatan</td>
						<td style="padding: 5px; text-align: left; width: 1%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="1%" align="left" bgcolor="transparent">:</td>
						<td style="padding: 5px; text-align: left; width: 82%; background-color: transparent; padding-top: 3px; padding-bottom: 0; vertical-align: top;" width="82%" align="left" bgcolor="transparent">
							{{ $transaction->note }}
						</td>
					</tr>
				</tbody>
			</table> -->
			<!-- <div id="notices" style="">
				<div style="text-align: center; margin-bottom: 10px"><b>&nbsp;</b></div>
				<div>Jakarta, {{ $transaction->tanggal }}</div>
				<div>Jam : {{ $transaction->jam }}</div>
				<div>Code : {{ strtoupper($transaction->code) }}</div>
			</div> -->
            <footer style="position: fixed; bottom: 1cm; height: 5.5cm;">
                @if($transaction->type == 1)
                    <div id="notices">
                        <table style="border:1px solid; padding: 5px;" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">KETERANGAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left; width: 20%;">ATAS NAMA</td>
                                    <td style="text-align: left; text-transform: capitalize;">: Atas nama</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 20%;">TANGGAL & WAKTU</td>
                                    <td style="text-align: left;">: {{ $transaction->tanggal }} {{ $transaction->jam }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="notices">
                        <div style="text-align: center; margin-bottom: 10px"><b>&nbsp;</b></div>
                        <table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 30px;" border="1" width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding: 5px; height: 1cm; background-color: transparent; width: 30%; text-align: center; text-transform: uppercase;" colspan="2" width="30%" bgcolor="transparent" align="center"><b>Penyerahan</b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;text-transform: uppercase;" width="30%" bgcolor="transparent" align="center"><b>YANG MENYERAHKAN</b></td>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;text-transform: uppercase;" width="30%" bgcolor="transparent" align="center"><b>YANG MENERIMA</b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; height: 1.75cm;" width="30%" bgcolor="transparent" align="center"></td>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; height: 1.75cm;" width="30%" bgcolor="transparent" align="center"></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; text-transform: uppercase;" width="30%" bgcolor="transparent" align="center">{{ $transaction->giver }}</td>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; text-transform: uppercase;" width="30%" bgcolor="transparent" align="center">{{ $transaction->receiver }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @else
                    <div id="notices">
                        <table style="border:1px solid; padding: 5px;" width="100%">
                            <thead>
                                <tr>
                                    <th style="text-align: left;">KETERANGAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-align: left; width: 20%;">ATAS NAMA</td>
                                    <td style="text-align: left; text-transform: capitalize;">: Atas nama</td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; width: 20%;">TANGGAL & WAKTU</td>
                                    <td style="text-align: left;">: {{ $transaction->tanggal }} {{ $transaction->jam }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="notices">
                        <div style="text-align: center; margin-bottom: 10px"><b>&nbsp;</b></div>
                        <table style="width: 100%; border-collapse: collapse; border-spacing: 0; margin-bottom: 30px;" border="1" width="100%">
                            <tbody>
                                <tr>
                                    <td style="padding: 5px; height: 1cm; background-color: transparent; width: 30%; text-align: center; text-transform: uppercase;" colspan="2" width="30%" bgcolor="transparent" align="center"><b>Pengembalian</b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;text-transform: uppercase;" width="30%" bgcolor="transparent" align="center"><b>YANG MENYERAHKAN</b></td>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center;text-transform: uppercase;" width="30%" bgcolor="transparent" align="center"><b>YANG MENERIMA</b></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; height: 1.75cm;" width="30%" bgcolor="transparent" align="center"></td>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; height: 1.75cm;" width="30%" bgcolor="transparent" align="center"></td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; text-transform: uppercase;" width="30%" bgcolor="transparent" align="center">{{ $transaction->giver }}</td>
                                    <td style="padding: 5px; background-color: transparent; width: 30%; text-align: center; text-transform: uppercase;" width="30%" bgcolor="transparent" align="center">{{ $transaction->receiver }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif
            </footer>
		</main>
    </body>
</html>
