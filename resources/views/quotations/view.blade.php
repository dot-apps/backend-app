@extends('layouts.adminLayout')

@section('title')
New Product
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/quotations">Quotations</a></li>
    <li class="breadcrumb-item active">Quotation Detail</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-12">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                
            </div>
            <div class="card-body">
                <div class="row">
                    <div id="card-linear-color" class="card card-primary col-md-6">
                        <div class="card-header  ">
                            <div class="card-title font-weight-bold">
                                Customer Data
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                @php
                                    $customer = json_decode($quotation['customers'],TRUE);
                                @endphp
                                <thead>
                                    <tr>
                                        <th style="width:20%">Name</th>
                                        <th>:</th>
                                        <th style="width:80%">
                                            {{$customer['customer_name']}}
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="width:10%">Phone</th>
                                        <th>:</th>
                                        <th style="width:90%">
                                            {{$customer['customer_phone']}}
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="width:10%">Email</th>
                                        <th>:</th>
                                        <th style="width:90%">
                                            {{$customer['customer_email']}}
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="width:10%">Address</th>
                                        <th>:</th>
                                        <th style="width:90%">
                                            {{$customer['customer_address']}}
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    
                    <div id="card-linear-color" class="card card-primary col-md-6">
                        <div class="card-header  ">
                            <div class="card-title font-weight-bold">Dates</div>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width:25%">Quotation Date</th>
                                        <th>:</th>
                                        <th style="width:75%">
                                            {{ $quotation->date }}
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="width:10%">Expired Date</th>
                                        <th>:</th>
                                        <th style="width:90%">
                                            {{ $quotation->expired_at }}
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <div id="card-linear-color" class="card card-primary col-md-12">
                        <div class="card-header  ">
                            <div class="card-title font-weight-bold">CART</div>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered" id="tblCart">
                                <thead>
                                    <tr>
                                        <th style="width:50%">Deksripsi</th>
                                        <th style="width:8%" class="text-center">Unit</th>
                                        <th style="width:8%" class="text-center">Hari</th>
                                        <th style="width:17%" class="text-center">Harga Satuan</th>
                                        <th style="width:17%" class="text-center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $carts = json_decode($quotation['carts'],TRUE);
                                        
                                    @endphp
                                    @foreach($carts as $_cart)
                                    <tr>
                                        <td>{!! $_cart['names'] !!}</td>
                                        <td>{!!$_cart['qtys'] !!}</td>
                                        <td>{!!$_cart['days'] !!}</td>
                                        <td class="text-right">Rp.{{number_format($_cart['prices'])}}</td>
                                        <td class="text-right">Rp.{{number_format($_cart['totals'])}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <table class="table table-bordered" id="tblCartSum">
                                <tbody>
                                    <!-- <tr>
                                        <td class="text-right font-weight-bold" style="width:76%">
                                            DISCOUNT FOR < 7 DAYS USES
                                        </td>
                                        <td class="total_discount" style="width: 16%">
                                            <input type="number" value="1500000" min="1" class="form-control text-center total_amount" name="product_total[]" readonly/>
                                        </td>
                                        <td style="width: 17%"></td>
                                    </tr> -->
                                    <tr>
                                        <td class="text-right font-weight-bold" style="width:83%">
                                            JUMLAH
                                        </td>
                                        <td class="text-right" style="width: 17%">
                                            Rp.{{number_format($quotation->subtotal)}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<style>
input[readonly] {
    background-color: #e5ebf9 !important;
    color:black !important; 
}
</style>
@endsection