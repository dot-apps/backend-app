@extends('layouts.adminLayout')

@section('title')
Edit User
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Master User</a></li>
    <li class="breadcrumb-item"><a href="{{ url('users' )}}">User List</a></li>
    <li class="breadcrumb-item active">Edit User</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Edit User</div>
            </div>
            <form method="POST" action={{url('users/' . $user->id)}} enctype="multipart/form-data">
            @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="full_name">Full Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value='{{$user->name}}'>
                        @error('name')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    @if ($user_role->role == 'operator')
                    <div class="form-group">
                        <label for="email">Phone</label>
                        <input type="number" name="phone" class="form-control @error('phone') is-invalid @enderror" id="phone" value='{{$user->phone}}'>
                        @error('phone')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value='{{$user->email}}'>
                        @error('email')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pass">New Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="pass">
                        @error('password')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="pass">Confirm New Password</label>
                        <input type="password" name="password_confirmation" class="form-control @error('password') is-invalid @enderror" id="pass">
                        @error('password')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" {{($user->gender == 'M')? "checked" : ""}} id="male" name="gender" value="M" class="custom-control-input" />
                            <label class="custom-control-label" for="male">Male</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" {{($user->gender == 'F')? "checked" : ""}} id="female" name="gender" value="F" class="custom-control-input" />
                            <label class="custom-control-label" for="female">Female</label>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <label for="role">Role</label>
                        <input type="text" name="role" class="form-control @error('role') is-invalid @enderror" id="role" value='{{$user_role->role}}'>
                        @error('role')
                            <div class="invalid-feedback">
                                    {{ $message }}
                            </div>
                        @enderror
                    </div> --}}
                    {{-- <div class="form-group">
                        <label for="permissionSelect">Permission</label>
                        <select class="form-control" name="permission" id="permissionSelect">
                            <option {{($user_role->permission == 'read')? 'selected': ''}} value="read">Read</option>
                            <option {{($user_role->permission == 'write')? 'selected': ''}} value="write">Write</option>
                            <option {{($user_role->permission == 'execute')? 'selected': ''}} value="execute">Execute</option>
                            <option {{($user_role->permission == 'delete')? 'selected': ''}} value="delete">Delete</option>
                        </select>
                    </div> --}}
                    <div class="form-group mb-0">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="image" name="image">
                            <label class="custom-file-label" for="image">Upload New Image</label>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-3 mr-4">
                            <a href="{{ url('users')}}" class="btn btn-secondary btn-block">Batal</a>
                        </div>
                        <div class="col-3">
                            <input type="hidden" name="_method" value="PUT">
                            <button type="submit" class="btn btn-primary btn-block">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
