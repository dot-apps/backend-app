@extends('layouts.adminLayout')

@section('title')
User List
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="#">Master User</a></li>
    <li class="breadcrumb-item active">User List</li>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Daftar Pengguna</div>
            </div>
            <div class="card-body mt-3">
                <table class="table table-users">
                    <thead class="thead-dark">
                        <tr>
                            <th class="text-white" scope="">#</th>
                            <th class="text-white" scope="col">Name</th>
                            <th class="text-white" scope="col">Email</th>
                            <th class="text-white" scope="col">Role</th>
                            <th class="text-white text-center text-no-wrap" scope="col">Created At</th>
                            <th class="text-white text-center" scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $no = 1 @endphp
                    @forelse ($users as $user)
                    <tr style="backgroundColor:#fff">
                        <td>{{ $no }}</td>
                        <td class="text-no-wrap">
                            {{$user->name}}
                        </td>
                        <td class="text-no-wrap">{{$user->email}}</td>
                        <td class="text-no-wrap">{{$user->role}}</td>
                        <td class="text-center text-no-wrap">{{$user->created_at}}</td>
                        <td class="justify-content-center text-center text-no-wrap">
                            <a href="{{'users/edit/'.$user->id}}" class="btn btn-warning btn-sm"><small>Edit</small></a>
                            <form action="{{url('users/'.$user->id)}}" method="POST" style="display:inline-block">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm text-light"><small>Delete</small></button>
                            </form>
                        </td>
                    </tr>
                    @php $no++; @endphp
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{ $users->links() }}
@endsection

@section('script')
<script>
    $('.table-users').DataTable();
</script>
@endsection
