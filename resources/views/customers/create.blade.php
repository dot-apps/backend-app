@extends('layouts.adminLayout')

@section('title')
New Customer
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="#">Pages</a></li>
    <li class="breadcrumb-item"><a href="/customers">Customers</a></li>
    <li class="breadcrumb-item active">New Customer</li>
@endsection

@section('content')
<div class="row">
    <div class=" col-md-6">
        <div id="card-linear-color" class="card card-default">
            <div class="card-header  ">
                <div class="card-title">Add New Customer</div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="/customers" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group form-group-default required">
                        <label for="full_name">Type</label>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio1" value="1" checked>
                            <label for="radio1">Personal</label>
                        </div>
                        <div class="form-check form-check-inline primary">
                            <input type="radio" name="type" id="radio2" value="2">
                            <label for="radio2">Corporatte</label>
                        </div>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="full_name" value="{{ old('name') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Phone</label>
                        <input type="text" name="phone" min=1000 class="form-control @error('phone') is-invalid @enderror" id="phone" value="{{ old('phone') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Email</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="Email" value="{{ old('email') }}" required>
                    </div>
                    <div class="form-group form-group-default required">
                        <label for="full_name">Address</label>
                        <textarea name="address" id="address" class="form-control" cols="30" rows="10">{{ old('address')}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
